/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.component;

import javafx.scene.control.Button;

/**
 *
 * @author Chris
 */
public class VideoComponent extends Component{
    public int width;
    public int height;
    public String caption;
    public String path;
    public String name;
    
     public VideoComponent(int w, int h, String c, String p){
         selected = true;
        width = w;
        height = h;
        caption = c;
        path = p;
        name = p;
        compButton = new Button("Video Component");
    }
}
