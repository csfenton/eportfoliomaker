/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.component;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Chris
 */
public class SlideShowModel {
    SlideShowMakerView ui;
    String title;
    ObservableList<Slide> slides;
    Slide selectedSlide;
    
    
    public SlideShowModel(SlideShowMakerView initUI) {
	ui = initUI;
        this.setTitle(ui.getTitle());
	slides = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
	//return selectedSlide != null;
        boolean selec = false;
        for (Slide slide : slides) {
	  if (slide.getSelected()){
              return true;
          } 
        }
        return false;
        
    }
    
    public int getSelectedSlideIndex(){
        for (Slide slide : slides) {
	  if (slide.getSelected()){
              return slides.indexOf(slide);
          } 
        }
        return -1;
    }
    
    public ObservableList<Slide> getSlides() {
        setCaption();
	return slides;
    }
    
    
    public void setCaption(){
        for (Slide slide : slides) {
	    
            slide.setCaption();
        }
    }
    
    
    public void selected(Slide selecslide){
        selectedSlide = selecslide;
        for (Slide slide : slides){
            slide.setSelected(false);
        }
        //selectedSlide.setSelected(true);
        
        
    }
    
    public Slide getSelectedSlide() {
	return selectedSlide;
    }

    public String getTitle() { 
        
        ui.reloadSlideShowPane(this);
	return ui.getTitle(); 
    }
    
    // MUTATOR METHODS
    public void setSelectedSlide(Slide initSelectedSlide) {
	selectedSlide = initSelectedSlide;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
        
	slides.clear();
	
	title = "Slide Show";
	selectedSlide = null;
    }

     public void reset(int i) {
        
	slides.clear();
	
	title = "Slide Show";
	selectedSlide = null;
        ui.reloadSlideShowPane(this);
    }
    
    
    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    public void addSlide(   String initImageFileName,
			    String initImagePath) {
	Slide slideToAdd = new Slide(initImageFileName, initImagePath);
	
        slides.add(slideToAdd);
	selectedSlide = slideToAdd;
	ui.reloadSlideShowPane(this);
        
    }
    
    
    //Removes the slide from the show
    public void removeSlide(String initImageFileName, String initImagePath, Slide selected){
      slides.remove(selected);
      ui.reloadSlideShowPane(this);
    }
    
    public void addSlide(   String initImageFileName,
			    String initImagePath, String cap) {
	Slide slideToAdd = new Slide(initImageFileName, initImagePath, cap);
	slides.add(slideToAdd);
	selectedSlide = slideToAdd;
	ui.reloadSlideShowPane(this);
    }
    
    
    
    // Moves a slide upwards by one
    public void moveSlideUp(Slide selected){
        Slide temp = selected;
        int index = slides.indexOf(selected);
        Slide upper = slides.get(index -1);
        removeSlide(selected.getImageFileName(), selected.getImagePath(), selected);
        slides.add(index-1, temp);
        ui.reloadSlideShowPane(this);
    }
    
    
    public void moveSlideDown(Slide selected){
        Slide temp = selected;
        int index = slides.indexOf(selected);
        Slide upper = slides.get(index +1);
        removeSlide(selected.getImageFileName(), selected.getImagePath(), selected);
        slides.add(index+1, temp);
        ui.reloadSlideShowPane(this);
    }
    
    
    
    
}
