/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.component;

import javafx.scene.control.Button;

/**
 *
 * @author Chris
 */
public class ParagraphComponent extends Component {
    public String paragraph;
    public String link;
    public String font;
    public int linkStart;
    public int linkEnd;
    
    
    public ParagraphComponent(String paragraph, String link, String font, int ls, int le){
        selected = true;
        this.paragraph = paragraph;
        this. link = link;
        this.font = font;
        linkStart =ls;
        linkEnd =le;
        name = "p";
        compButton = new Button("Paragraph Component");
    }
}
