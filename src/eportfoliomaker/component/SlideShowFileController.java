/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.image.ImageView;

/**
 *
 * @author Chris
 */
public class SlideShowFileController {
     // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;
    public static String showTitle = "a";
    // THE APP UI
    private SlideShowMakerView ui;
    
    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private SlideShowFileManager slideShowIO;
    String SLASH = "/";
    
    ImageView imageSelectionView;
    /**
     * This default constructor starts the program without a slide show file being
     * edited.
     *
     * @param initSlideShowIO The object that will be reading and writing slide show
     * data.
     */
    public SlideShowFileController(SlideShowMakerView initUI, SlideShowFileManager initSlideShowIO) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        slideShowIO = initSlideShowIO;
    }
    
    
    
    
    
     public boolean handleSaveSlideShowRequest(SlideShowMakerView show) {
        try {
	    // GET THE SLIDE SHOW TO SAVE
	    SlideShowModel slideShowToSave = ui.getSlideShow();
	    System.out.println("adsfasdf0");
            // SAVE IT TO A FILE
            slideShowIO.saveSlideShow(slideShowToSave);

            // MARK IT AS SAVED
            //saved = true;

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            //ui.updateToolbarControls(saved);
            
            //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
             String title = (show.getTitle());
             System.out.println("adsfasdf0");
        showTitle = title;
        File slideDirectory = new File("./src/eportfoliomaker/sites/" + SLASH+ title + SLASH + "slideShow.json");
        if (slideDirectory.exists()){
            slideDirectory.delete();
            System.out.println("zzzzzzzzzzzzzzzzzzzzzzzz");
        }
        else{
            System.out.println("sadljfhsad;ljfh");
        }
        new File("./src/eportfoliomaker/sites/"+ SLASH+ title).mkdirs();
        new File("./src/eportfoliomaker/sites/" + SLASH+ title +SLASH+ "img").mkdirs();
        new File("./src/eportfoliomaker/sites/" + SLASH+ title +SLASH+ "css").mkdirs();
        new File("./src/eportfoliomaker/sites/" + SLASH+ title +SLASH+ "js").mkdirs();
        new File("./src/eportfoliomaker/sites/"+ SLASH+ title +SLASH+ "icons").mkdirs();
        SlideShowModel model = show.getSlideShow();
        ObservableList<Slide> slides = model.getSlides();
        ArrayList<String> slidePics = new ArrayList<String>();
        //copy over all the pictures
        for (Slide shower: slides){
            Path sourceIMG = Paths.get(shower.getImagePath() +  SLASH + shower.getImageFileName());
            Path destIMG = Paths.get("./src/eportfoliomaker/sites/"+ SLASH+ title +SLASH+ "img");
            slidePics.add("./src/eportfoliomaker/sites/" + SLASH+ title +SLASH+ "img" +SLASH+shower.getImageFileName());
            try {
                java.nio.file.Files.copy(sourceIMG, destIMG.resolve(shower.getImageFileName()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //copy over json file, and rename it
        Path sourceJSON = Paths.get("./data/slide_shows" + SLASH + title + ".json");
        Path destJSON = Paths.get("./src/eportfoliomaker/sites/" + SLASH+ title);
        try {
                java.nio.file.Files.copy(sourceJSON, destJSON.resolve("slideShow.json"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        
        
        //copy over jquery
       
        Path sourceJQ = Paths.get("./JS/jquery-2.1.4.min.js");
        Path destJQ = Paths.get("./src/eportfoliomaker/sites/" + SLASH+ title + SLASH + "js");
        try {
                java.nio.file.Files.copy(sourceJQ, destJQ.resolve("jquery-2.1.4.min.js"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        
        
        
        //copy over the css
        Path sourceCSS = Paths.get("./HTML/public_html/ViewerCss.css");
        Path destCSS = Paths.get("./src/eportfoliomaker/sites/" + SLASH+ title + SLASH + "css");
        try {
                java.nio.file.Files.copy(sourceCSS, destCSS.resolve("ViewerCss.css"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        
         //copy over the js
        Path sourceJS = Paths.get("./JS/ssmjs.js");
        Path destJS = Paths.get("./src/eportfoliomakersites/" + SLASH+ title + SLASH + "js");
        try {
                java.nio.file.Files.copy(sourceJS, destJS.resolve("ssmjs.js"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        
        
         //copy over the HTML
        Path sourceHTML = Paths.get("./HTML/index.html");
        Path destHTML = Paths.get("./src/eportfoliomaker/sites/" + SLASH+ title);
        try {
                java.nio.file.Files.copy(sourceHTML, destHTML.resolve("index.html"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        
        
        
        //copy over the icons
        File sourceIcons = new File("./images/icons");
        File destIcons = new File("./src/eportfoliomaker/sites/" + SLASH+ title + SLASH + "icons");
        try {
                copyFiles(sourceIcons, destIcons);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            
	    return true;
        } catch (IOException ioe) {
          
	    return false;
        }
    }
     
      public void copyFiles(File src, File dest) throws IOException{
        if(src.isDirectory()){
    		
    		//if directory not exists, create it
    		if(!dest.exists()){
    		   dest.mkdir();
    		   System.out.println("Directory copied from " 
                              + src + "  to " + dest);
    		}
    		
    		//list all the directory contents
    		String files[] = src.list();
    		
    		for (String file : files) {
    		   //construct the src and dest file structure
    		   File srcFile = new File(src, file);
    		   File destFile = new File(dest, file);
    		   //recursive copy
    		   copyFiles(srcFile,destFile);
    		}
    	   
    	}else{
    		//if file, then copy it
    		//Use bytes stream to support all file types
    		InputStream in = new FileInputStream(src);
    	        OutputStream out = new FileOutputStream(dest); 
    	                     
    	        byte[] buffer = new byte[1024];
    	    
    	        int length;
    	        //copy the file content in bytes 
    	        while ((length = in.read(buffer)) > 0){
    	    	   out.write(buffer, 0, length);
    	        }
 
    	        in.close();
    	        out.close();
    	        System.out.println("File copied from " + src + " to " + dest);
    	}
    }
}
