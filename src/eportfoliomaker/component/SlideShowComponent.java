/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.component;

import javafx.collections.ObservableList;
import javafx.scene.control.Button;

/**
 *
 * @author Chris
 */
public class SlideShowComponent extends Component{
    public ObservableList<Slide> slides;
    public SlideShowMakerView ssmv;
    public SlideShowModel ssm;
    public SlideShowEditController ssec;
    
    public SlideShowComponent(ObservableList<Slide> s, SlideShowMakerView ssmv){
        slides = s;
        compButton = new Button("Slide Show Component");
        name = "ssc";
        selected = true;
        this.ssmv = ssmv;
    }
}
