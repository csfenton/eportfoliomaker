/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.component;

/**
 *
 * @author Chris
 */
public class Slide {
    String imageFileName;
    String imagePath;
    String caption;
    SlideEditView editor;
    boolean selected;
     
    /**
     * Constructor, it initializes all slide data.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     */
    public Slide(String initImageFileName, String initImagePath) {
	imageFileName = initImageFileName;
	imagePath = initImagePath;
    }
    
     public Slide(String initImageFileName, String initImagePath, String cap) {
	imageFileName = initImageFileName;
	imagePath = initImagePath;
        caption = cap;
    }
    
    // ACCESSOR METHODS
    public String getImageFileName() { return imageFileName; }
    public String getImagePath() { return imagePath; }
    public String getCaption() { return caption;  }
    
    public boolean getSelected(){
        return selected;
    }
    
    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
	imageFileName = initImageFileName;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setImage(String initPath, String initFileName) {
	imagePath = initPath;
	imageFileName = initFileName;
    }
    
    public void setEditor(SlideEditView ed){
        editor =ed;
    }
    
    public void setCaption(String cap){
        caption = cap;
    }
    
    public void setCaption(){
        if (editor != null){
            caption = editor.getCaption();
        }
    }
     public void setSelected(boolean s){
         selected = s;
         if (s == true){
             //editor.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, null, null)));
             editor.setOpacity(.50);
             
         }
         
         if (selected == false){
             editor.setOpacity(10);
         }
     }
    
     
     
     
}
