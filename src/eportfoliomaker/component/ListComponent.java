/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.component;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;

/**
 *
 * @author Chris
 */
public class ListComponent extends Component{
     public ObservableList<ListItem> items = FXCollections.observableArrayList();
    
     public ListComponent(ObservableList<ListItem> l){
         selected = true;
         compButton= new Button("List Component");
         for (ListItem item: l){
             items.add(item);
         }
         name = "a";
     }
    
}
