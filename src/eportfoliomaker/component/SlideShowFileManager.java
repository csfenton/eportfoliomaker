/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;

/**
 *
 * @author Chris
 */
public class SlideShowFileManager {
    String SLASH = "/";
     public void saveSlideShow(SlideShowModel slideShowToSave) throws IOException {
         
         
        // BUILD THE FILE PATH
        //slideShowToSave.setTitle(slideShowToSave.;
        String slideShowTitle = "aa" + slideShowToSave.getTitle();
        String jsonFilePath = "./data/slide_shows" + SLASH + slideShowTitle + ".json";
        System.out.println(jsonFilePath);
        
        // INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        
        JsonWriter jsonWriter = Json.createWriter(os);  
           
        // BUILD THE SLIDES ARRAY
        JsonArray slidesJsonArray = makeSlidesJsonArray(slideShowToSave.getSlides());
        
        // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        JsonObject courseJsonObject = Json.createObjectBuilder()
                                    .add("title", slideShowToSave.getTitle())
                                    .add("slides", slidesJsonArray)
                                    
                .build();
        
        // AND SAVE EVERYTHING AT ONCE
        
        jsonWriter.writeObject(courseJsonObject);
    }
     
     
     private JsonArray makeSlidesJsonArray(List<Slide> slides) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Slide slide : slides) {
	    JsonObject jso = makeSlideJsonObject(slide);
	    jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;        
    }
    
    private JsonObject makeSlideJsonObject(Slide slide) {
        String cap;
        if (slide.getCaption() == null) {
            slide.setCaption("");}
        else {cap = "0";}
        JsonObject jso = Json.createObjectBuilder()
		.add("image_file_name", slide.getImageFileName())
		.add("image_path", slide.getImagePath())
                .add("caption", slide.getCaption())
		.build();
	return jso;
    }
}

