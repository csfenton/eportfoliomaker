/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.component;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 *
 * @author Chris
 */
public class ListItem {
    public String item;
    public Button removeButton = new Button("Remove List Item");
    public TextField listText = new TextField();
    
    public ListItem(){
        
    }
    
    public ListItem(String it){
        item = it;
        listText.setText(it);
    }
    
    public void setItem(String item){
        this.item = item;
    }
    
    public String getItem(){
        return item;
    }
    
    
}
