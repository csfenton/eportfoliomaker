/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.component;

/**
 *
 * @author Chris
 */
public class SlideShowEditController {
    // APP UI
    private SlideShowMakerView ui;
    
    /**
     * This constructor keeps the UI for later.
     */
    public SlideShowEditController(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddSlideRequest() {
	SlideShowModel slideShow = ui.getSlideShow();
	
	slideShow.addSlide("DefaultStartSlide.png", "./images/slide_show_images/");
        ui.updateToolbarControls(false);
    }
    
    public void processRemoveSlideRequest(Slide slide){
        SlideShowModel slideShow = ui.getSlideShow();
	
	slideShow.removeSlide("DefaultStartSlide.png", "./images/slide_show_images/", slide);
        ui.updateToolbarControls(false);
    }
    
    public void processMoveUpSlideRequest(Slide slide){
        SlideShowModel slideShow = ui.getSlideShow();
        slideShow.moveSlideUp(slide);
        ui.updateToolbarControls(false);
    }
    
    public void processMoveDownSlideRequest(Slide slide){
        SlideShowModel slideShow = ui.getSlideShow();
        slideShow.moveSlideDown(slide);
        ui.updateToolbarControls(false);
    }
}
