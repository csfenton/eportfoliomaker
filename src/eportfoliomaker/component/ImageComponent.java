/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.component;

import javafx.scene.control.Button;

/**
 *
 * @author Chris
 */
public class ImageComponent extends Component {
    public int width;
    public int height;
    public String caption;
    public String floated;
    public String path;
    public String name;
    
    public ImageComponent(int w, int h, String c, String f, String p){
        width = w;
        height = h;
        caption = c;
        floated = f;
        path = p;
        name = p;
        compButton = new Button("Image Component");
        selected = true;
    }
    
}
