/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.component;

import eportfoliomaker.view.PageEditView;
import javafx.geometry.Orientation;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class SlideShowMakerView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    
    Button newSlideShowButton;
    Button loadSlideShowButton;
    Button saveSlideShowButton;
    Button viewSlideShowButton;
    Button exitButton;
    
    // WORKSPACE
    HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    FlowPane slideEditToolbar;
    Button addSlideButton;
    Button moveUpButton;
    Button moveDownButton;
    Button removeSlideButton;
    
    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    
    VBox slidesEditorPane;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideShowModel slideShow;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager fileManager = new SlideShowFileManager();
    

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
   // private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private SlideShowFileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    public SlideShowEditController editController;
    
    //enter a title
    TextField title = new TextField();
    
    String stringTitle = title.getText();
    public String getTitle(){
        return stringTitle;
    }
    
    public TextField gettf(){
        return title;
    }
    
    public void setTitle(String initTitle){
        title.setText(initTitle);
    }
    
    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public SlideShowMakerView() {
	// FIRST HOLD ONTO THE FILE MANAGER
	
	
	// MAKE THE DATA MANAGING MODEL
	slideShow = new SlideShowModel(this);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	
        
        
        
        
    }
    
    
    
    
    // ACCESSOR METHODS
    public SlideShowModel getSlideShow() {
	 if (slideShow.isSlideSelected()){
            removeSlideButton.setDisable(false);
            if (slideShow.getSelectedSlideIndex() == 0){ moveUpButton.setDisable(true);}
            else{moveUpButton.setDisable(false);}
            if (slideShow.getSelectedSlideIndex() == slideShow.getSlides().size()-1){moveDownButton.setDisable(true);}
            else {moveDownButton.setDisable(false);}
        }
         else { 
             removeSlideButton.setDisable(true);
             moveUpButton.setDisable(true);
             moveDownButton.setDisable(true);
         }
        return slideShow;
       
    }

    public Stage getWindow() {
	return primaryStage;
    }

   

    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle, PageEditView pev) {
	// THE TOOLBAR ALONG THE NORTH
	

        ssmPane= new BorderPane();
        
        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();
        updateToolbarControls(false);
	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle, pev);
       
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
	workspace.getStyleClass().add("work-space");
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
        Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();
	slideEditToolbar = new FlowPane(Orientation.VERTICAL);
        slideEditToolbar.setVgap((bounds.getHeight() /4)  +3);
	slideEditToolbar.getStyleClass().add("tool_bar");
	addSlideButton = this.initChildButton(slideEditToolbar,		"Add.png",	    "Add Slide",	    "vertical_toolbar_button",  true);
        moveUpButton = this.initChildButton(slideEditToolbar,		"MoveUp.png",	   "Move Up",	    "vertical_toolbar_button",  true);
        moveDownButton = this.initChildButton(slideEditToolbar,		"MoveDown.png",	    "Move Down",	    "vertical_toolbar_button",  false);
        removeSlideButton = this.initChildButton(slideEditToolbar,	"Remove.png",	    "Remove Slide",	    "vertical_toolbar_button",  true);
	
	// AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox();
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
       // slidesEditorScrollPane.getStyleClass().add("work_space");
	//slidesEditorPane.getStyleClass().add("work_space");
	// NOW PUT THESE TWO IN THE WORKSPACE
	workspace.getChildren().add(slideEditToolbar);
	workspace.getChildren().add(slidesEditorScrollPane);
    }

    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	
	
	// THEN THE SLIDE SHOW EDIT CONTROLS
	editController = new SlideShowEditController(this);
	addSlideButton.setOnAction(e -> {
	    editController.processAddSlideRequest();
            
	});
        
        removeSlideButton.setOnAction(e -> {
            editController.processRemoveSlideRequest(slideShow.getSelectedSlide());
            removeSlideButton.setDisable(true);
            
        });
        
        //MOVE THE SLIDE UP
        moveUpButton.setOnAction(e -> {
            editController.processMoveUpSlideRequest(slideShow.getSelectedSlide());
            moveUpButton.setDisable(true);
             moveDownButton.setDisable(true);
             
             removeSlideButton.setDisable(true);
        });
        
        //MOVE THE SLIDE DOWN
        moveDownButton.setOnAction(e -> {
            editController.processMoveDownSlideRequest(slideShow.getSelectedSlide());
            moveDownButton.setDisable(true);
            moveUpButton.setDisable(true);
            removeSlideButton.setDisable(true);
            
        });
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    
    
    

    private void initWindow(String windowTitle, PageEditView pev) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();
        
	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        Button okButton = new Button("Okay");
        ssmPane.setBottom(okButton);
        
        
        //Save all slide data in to Slide Show Component
        okButton.setOnAction(e ->{
            pev.slides = slideShow.getSlides();
            pev.slideShowMakerView = this;
             titleWindow();
            primaryStage.close();
           
        });
        
        
        
        //Image image = new Image("ssmicon.png");
        HBox pic = new HBox();
        //pic.getChildren().add(image);
        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	//ssmPane = new BorderPane();
	
        //ssmPane.setCenter(new ImageView(image));
            //add space to enter title
                
                Label l =new Label("Slide Show Component");
                //l.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON);
                //fileToolbarPane.getChildren().addAll(l, title);
                
                stringTitle = title.getText();
                
                
	primaryScene = new Scene(ssmPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
	primaryStage.setScene(primaryScene);
        if (slideShow != null){
            reloadSlideShowPane(slideShow);
        }
        
        
	primaryStage.showAndWait();
    }
    
    public void titleWindow(){
        Stage titleStage = new Stage();
        
        fileController = new SlideShowFileController(this, fileManager);
        
        Label headerLabel = new Label("Enter a title");
        TextField headerText = new TextField();
        headerText.setMinWidth(300);
        Button ok = new Button("Okay");
       Pane p = new Pane();
         p.getChildren().add(headerLabel);
       // p.add(new Label(""), 10, 15);
        p.getChildren().add(headerText);
        p.getChildren().add(ok);
        // p.add(new Label(""), 10, 55);
        
        headerLabel.setLayoutY(50);
        headerLabel.setLayoutX(120);
        
         headerText.setLayoutY(70);
        headerText.setLayoutX(25);
        
         ok.setLayoutY(100);
        ok.setLayoutX(145);
        
        
        
         p.getStyleClass().add("btYES");
        Scene scene = new Scene(p, 350, 180);
        scene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
      
        titleStage.setTitle("Enter");
        titleStage.setScene(scene);
        
        
        
        ok.setOnAction(e -> {
            //Component comp = new Component();
            //HeaderComponent header = new HeaderComponent(headerText.getText());
           
            String title = headerText.getText();
            slideShow.title = title;
             titleStage.close();
             fileController.handleSaveSlideShowRequest(this);
        });
        
        
        titleStage.showAndWait();
    }
    
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String tooltip, 
	    String cssClass,
	    boolean disabled) {
	
	String imagePath = "file:" + "./images/icons/" + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    
    /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	ssmPane.setCenter(workspace);
        //Image image = new Image("ssmicon.png");
	//ssmPane.setRight(new ImageView(image));
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	
	
	
	// AND THE SLIDESHOW EDIT TOOLBAR
	addSlideButton.setDisable(false);
    }

    /**
     * Uses the slide show data to reload all the components for
     * slide editing.
     * 
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSlideShowPane(SlideShowModel slideShowToLoad) {
	slidesEditorPane.getChildren().clear();
	for (Slide slide : slideShowToLoad.getSlides()) {
	    SlideEditView slideEditor = new SlideEditView(slide, this);
            //slide.setCaption(slideEditor.getCaption());
            slideEditor.setCaption(slide.getCaption());
	    slidesEditorPane.getChildren().add(slideEditor);
            if (slideShow.isSlideSelected()){
                removeSlideButton.setDisable(false);
            }
            else { removeSlideButton.setDisable(true);}
	}
        if (slideShow.isSlideSelected()){
            moveUpButton.setDisable(false);
        }
        
        stringTitle = title.getText();
        if (stringTitle.length() > 0){
            slideShowToLoad.setTitle(stringTitle);
            
        }
    }
}