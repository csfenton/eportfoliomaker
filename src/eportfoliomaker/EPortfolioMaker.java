/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import eportfoliomaker.file.EPortfolioFileManager;
import eportfoliomaker.view.EPortfolioMakerView;


/**
 *
 * @author Chris
 */
public class EPortfolioMaker extends Application {
    
     // THIS WILL PERFORM PORTFOLIO READING AND WRITING
    EPortfolioFileManager fileManager = new  EPortfolioFileManager();

    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    EPortfolioMakerView ui = new EPortfolioMakerView(fileManager);
    
    
    
    
    @Override
    public void start(Stage primaryStage) {
       
        
        
        ui.startUI(primaryStage, "ePortfolioMaker");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
