/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.file;

import eportfoliomaker.component.Component;
import eportfoliomaker.component.HeaderComponent;
import eportfoliomaker.component.ImageComponent;
import eportfoliomaker.component.ListComponent;
import eportfoliomaker.component.ListItem;
import eportfoliomaker.component.ParagraphComponent;
import eportfoliomaker.component.Slide;
import eportfoliomaker.component.SlideShowComponent;
import eportfoliomaker.component.VideoComponent;
import eportfoliomaker.model.EPortfolioModel;
import eportfoliomaker.model.Page;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author Chris
 */
public class EPortfolioFileManager {
    // JSON FILE READING AND WRITING CONSTANTS
    public static String JSON_TITLE = "title";
    public static String JSON_SLIDES = "slides";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";
    ObservableList<Component> cat;
    
    
     public void loadPortfolio(EPortfolioModel portfolioToLoad, String jsonFilePath) throws IOException {
        portfolioToLoad.reset();
             // LOAD THE JSON FILE WITH ALL THE DATA
            JsonObject json = loadJSONFile(jsonFilePath);

            // NOW LOAD THE COURSE
            ObservableList<String> pages = FXCollections.observableArrayList();
            //portfolioToLoad.setTitle(json.getString(JSON_TITLE));
            JsonArray jsonPageArray = json.getJsonArray("pages");
            for (int i = 0; i < jsonPageArray.size(); i++) {
                JsonObject slideJso = jsonPageArray.getJsonObject(i);
                pages.add(slideJso.getString("location") );
            }
            loadPage(portfolioToLoad, pages);
    }
     
     public void loadPage(EPortfolioModel portfolioToLoad, ObservableList<String> jsonFilePath){
         for (String s: jsonFilePath){
             try{
                Page page = new Page();
                JsonObject json = loadJSONFile(s);
                
                JsonArray jsonContentArray = json.getJsonArray("content");
                loadContent(portfolioToLoad, jsonContentArray, page);
               
                
                
                page.setTitle(json.getString("page_name"));
                page.setBannerImage(json.getString("bannerImage"));
                page.setLayout(json.getString("layout"));
                page.setFont(json.getString("font"));
                page.setColor(json.getString("style"));
               // page.components=null;
                portfolioToLoad.addPage(page);
                 JsonArray jsonPageArray = json.getJsonArray("pages");
             }catch (Exception e){System.out.println("Error here");}
         }
     }
     
     public void loadContent(EPortfolioModel portfolioToLoad, JsonArray contentArray, Page page){
          for (int i = 0; i < contentArray.size(); i++) {
                    JsonObject slideJso = contentArray.getJsonObject(i);
                    String type = slideJso.getString("type");
                    if (type.equalsIgnoreCase("h")){
                        HeaderComponent hc = new HeaderComponent(slideJso.getString("text"));
                        page.components.add(hc);
                    }
                    if (type.equalsIgnoreCase("p")){
                        
                        ParagraphComponent pc = new ParagraphComponent(slideJso.getString("text"), slideJso.getString("font"), "HyperLink", 0, 0);
                        page.components.add(pc);
                    }
                    if (type.equalsIgnoreCase("i")){
                        ImageComponent ic = new ImageComponent(slideJso.getInt("width"), slideJso.getInt("height"), slideJso.getString("caption"), slideJso.getString("style"), slideJso.getString("text"));
                        page.components.add(ic);
                    }
                     if (type.equalsIgnoreCase("v")){
                        VideoComponent vc = new VideoComponent(slideJso.getInt("width"), slideJso.getInt("height"), slideJso.getString("caption"), slideJso.getString("text"));
                        page.components.add(vc);
                    }
                     if (type.equalsIgnoreCase("l")){
                         int size = slideJso.getInt("text");
                         ObservableList<ListItem> item = FXCollections.observableArrayList();
                         if (size == 1){
                             ListItem li = new ListItem(slideJso.getString("first"));
                             item.add(li);
                             ListComponent lc = new ListComponent(item);
                         page.components.add(lc);
                         }
                         if (size == 2){
                             ListItem li = new ListItem(slideJso.getString("first"));
                             item.add(li);
                             ListItem lq = new ListItem(slideJso.getString("second"));
                             item.add(li);
                             item.add(lq);
                             ListComponent lc = new ListComponent(item);
                         page.components.add(lc);
                         }
                         if (size == 3){
                             ListItem li = new ListItem(slideJso.getString("first"));
                             item.add(li);
                             ListItem lq = new ListItem(slideJso.getString("second"));
                             item.add(lq);
                             ListItem lw = new ListItem(slideJso.getString("third"));
                             item.add(lw);
                             ListComponent lc = new ListComponent(item);
                         page.components.add(lc);
                         }
                         if (size == 4){
                             ListItem li = new ListItem(slideJso.getString("first"));
                             item.add(li);
                              ListItem lq = new ListItem(slideJso.getString("second"));
                             item.add(lq);
                             ListItem lw = new ListItem(slideJso.getString("third"));
                             item.add(lw);
                             ListItem le = new ListItem(slideJso.getString("fourth"));
                             item.add(le);
                             ListComponent lc = new ListComponent(item);
                         page.components.add(lc);
                         }
                         if (size == 5){
                             ListItem li = new ListItem(slideJso.getString("first"));
                             item.add(li);
                             ListItem lq = new ListItem(slideJso.getString("second"));
                             item.add(lq);
                             ListItem lw = new ListItem(slideJso.getString("third"));
                             item.add(lw);
                             ListItem le = new ListItem(slideJso.getString("fourth"));
                             item.add(le);
                             ListItem lr = new ListItem(slideJso.getString("fifth"));
                             item.add(lr);
                             ListComponent lc = new ListComponent(item);
                         page.components.add(lc);
                         }
                         if (size == 6){
                             ListItem li = new ListItem(slideJso.getString("first"));
                             item.add(li);
                             ListItem lq = new ListItem(slideJso.getString("second"));
                             item.add(lq);
                             ListItem lw = new ListItem(slideJso.getString("third"));
                             item.add(lw);
                             ListItem le = new ListItem(slideJso.getString("fourth"));
                             item.add(le);
                             ListItem lr = new ListItem(slideJso.getString("fifth"));
                             item.add(lr);
                             ListItem lt = new ListItem(slideJso.getString("sixth"));
                             item.add(lt);
                             ListComponent lc = new ListComponent(item);
                         page.components.add(lc);
                         }
                         if (size == 7){
                             ListItem li = new ListItem(slideJso.getString("first"));
                             item.add(li);
                             ListItem lq = new ListItem(slideJso.getString("second"));
                             item.add(lq);
                             ListItem lw = new ListItem(slideJso.getString("third"));
                             item.add(lw);
                             ListItem le = new ListItem(slideJso.getString("fourth"));
                             item.add(le);
                             ListItem lr = new ListItem(slideJso.getString("fifth"));
                             item.add(lr);
                             ListItem lt = new ListItem(slideJso.getString("sixth"));
                             item.add(lt);
                             ListItem ly = new ListItem(slideJso.getString("seventh"));
                             item.add(ly);
                             ListComponent lc = new ListComponent(item);
                         page.components.add(lc);
                         }
                         
                     }
                  if (type.equalsIgnoreCase("f")){
                       page.setFooterText(slideJso.getString("text"));
                    }
                }
     }

    // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }    
    
    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }
    /////////////////////////////////////////////////////////////////////////////
    ////////////////////////EXPORTING THE PORTFOLIO/////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    public void exportPortfolio(EPortfolioModel ePortfolioToSave) throws IOException{
        //
        
        String title = (ePortfolioToSave.saveName);
        File slideDirectory = new File("./data/exports/" + SLASH+ title);
        if (slideDirectory.exists()){
            slideDirectory.delete();
            System.out.println("zzzzzzzzzzzzzzzzzzzzzzzz");
        }
        else{
            System.out.println("sadljfhsad;ljfh");
        }
        new File("./data/exports/"+ SLASH+ title).mkdirs();
        new File("./data/exports/" + SLASH+ title +SLASH+ "img").mkdirs();
        new File("./data/exports/" + SLASH+ title +SLASH+ "videos").mkdirs();
        new File("./data/exports/" + SLASH+ title +SLASH+ "js").mkdirs();
        new File("./data/exports/"+ SLASH+ title +SLASH+ "icons").mkdirs();
        
        ///COPY OVER ALL THE PICTURES AND VIDEOS///
        ObservableList<Page> pages = ePortfolioToSave.getPages();
        for(Page page: pages){
            
                //copy over the HTML
            Path sourceHTML = Paths.get("./data/exports/page1.html");
            Path destHTML = Paths.get("./data/exports/" + SLASH+ title);
            try {
                    java.nio.file.Files.copy(sourceHTML, destHTML.resolve(page.getTitle() +".html"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            
            //COPY OVER BANNER IMAGE
            if (page.getBannerImage() != null){
            Path sourceBI = Paths.get(page.getBannerImage());
            Path destBI = Paths.get("./data/exports/" + SLASH+ title + SLASH + "img");
            try {
                    java.nio.file.Files.copy(sourceBI, destBI.resolve(page.getBannerName()));
                    page.setBannerImage("img" + SLASH + page.getBannerName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            ObservableList<Component> components = page.components;
            for (Component comp: components){
                if (comp instanceof ImageComponent){
                    ImageComponent ic = (ImageComponent) comp;
                      Path sourceIMG = Paths.get(ic.path);
                      Path destIMG = Paths.get("./data/exports/"+ SLASH+ title +SLASH+ "img");
                     
                      try {
                        java.nio.file.Files.copy(sourceIMG, destIMG.resolve(ic.name));
                        ic.path =  "img" + SLASH+ ic.name;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } 
                
                
                if (comp instanceof VideoComponent){
                    VideoComponent vc = (VideoComponent) comp;
                      Path sourceIMG = Paths.get(vc.path);
                      Path destIMG = Paths.get("./data/exports/"+ SLASH+ title +SLASH+ "videos");
                     
                      try {
                        java.nio.file.Files.copy(sourceIMG, destIMG.resolve(vc.name));
                        vc.path = "videos" +SLASH+ vc.name;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } 
                
                if (comp instanceof SlideShowComponent){
                    SlideShowComponent ssc = (SlideShowComponent) comp;
                    ObservableList<Slide> slides = ssc.slides;
                    for (Slide shower: slides){
                        Path sourceIMG = Paths.get(shower.getImagePath() +  SLASH + shower.getImageFileName());
                        Path destIMG = Paths.get("./data/exports/"+ SLASH+ title +SLASH+ "img");
                       
                        try {
                            java.nio.file.Files.copy(sourceIMG, destIMG.resolve(shower.getImageFileName()));
                            shower.setImagePath("./data/exports/"+ SLASH+ title +SLASH+ "img");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                
                
            }
            
            
            
            
        }
        
        saveSlideShow(ePortfolioToSave);
            //COPY OVER THE GENERATED JSON FILES
        File sourceJSON = new File("./data/portfolios" +SLASH + title);
        File destJSON = new File("./data/exports/" + SLASH+ title + SLASH + "json");
        try {
                copyFiles(sourceJSON, destJSON);
            } catch (IOException e) {
                e.printStackTrace();
            }
        
        
         //copy over the js
        Path sourceJS = Paths.get("./data/exports/js/pageReader.js");
        Path destJS = Paths.get("./data/exports/" + SLASH+ title + SLASH + "js");
        try {
                java.nio.file.Files.copy(sourceJS, destJS.resolve("pageReader.js"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        
        //COPY OVER ALL THE ICONS
        File sourceIcons = new File("./images/icons");
        File destIcons = new File("./data/exports/" + SLASH+ title + SLASH + "icons");
        try {
                copyFiles(sourceIcons, destIcons);
            } catch (IOException e) {
                e.printStackTrace();
            }
        
         //COPY OVER ALL THE StyleSheets
        File sourceCSS = new File("./data/exports/css");
        File destCSS = new File("./data/exports/" + SLASH+ title + SLASH + "css");
        try {
                copyFiles(sourceCSS, destCSS);
            } catch (IOException e) {
                e.printStackTrace();
            }
        
        
}
     public void copyFiles(File src, File dest) throws IOException{
        if(src.isDirectory()){
    		
    		//if directory not exists, create it
    		if(!dest.exists()){
    		   dest.mkdir();
    		   System.out.println("Directory copied from " 
                              + src + "  to " + dest);
    		}
    		
    		//list all the directory contents
    		String files[] = src.list();
    		
    		for (String file : files) {
    		   //construct the src and dest file structure
    		   File srcFile = new File(src, file);
    		   File destFile = new File(dest, file);
    		   //recursive copy
    		   copyFiles(srcFile,destFile);
    		}
    	   
    	}else{
    		//if file, then copy it
    		//Use bytes stream to support all file types
    		InputStream in = new FileInputStream(src);
    	        OutputStream out = new FileOutputStream(dest); 
    	                     
    	        byte[] buffer = new byte[1024];
    	    
    	        int length;
    	        //copy the file content in bytes 
    	        while ((length = in.read(buffer)) > 0){
    	    	   out.write(buffer, 0, length);
    	        }
 
    	        in.close();
    	        out.close();
    	        System.out.println("File copied from " + src + " to " + dest);
    	}
    }
    /**
     * This method saves all the data associated with a slide show to
     * a JSON file.
     * 
     * @param slideShowToSave The course whose data we are saving.
     * 
     * @throws IOException Thrown when there are issues writing
     * to the JSON file.
     */
    public void saveSlideShow(EPortfolioModel ePortfolioToSave) throws IOException {
        // BUILD THE FILE PATH
        //slideShowToSave.setTitle(slideShowToSave.;
        
        String slideShowTitle = "" + ePortfolioToSave.saveName;
        ObservableList<Page> pages = ePortfolioToSave.getPages();
        new File("./data/portfolios/" + SLASH  + slideShowTitle).mkdir();
        int i = 0;
        ObservableList<String> loader = FXCollections.observableArrayList();
        for (Page page: pages){
            String pageTitle = "" + page.getTitle();
            loader.add(pageTitle);
            String jsonFilePath = "./data/portfolios/" + SLASH  + slideShowTitle + SLASH + pageTitle+ JSON_EXT;
            String jsonFolderPath = "./data/portfolios/" + SLASH  + slideShowTitle + SLASH +pageTitle +SLASH + "page"+ JSON_EXT;
            i++;
            System.out.println(jsonFilePath);
            // INIT THE WRITER
            OutputStream os = new FileOutputStream(jsonFilePath);
                System.out.println("Here");
            //JsonWriter jsonWriter = Json.createWriter(os);  
            // BUILD THE SLIDES ARRAY
            JsonArray slidesJsonArray = makeComponentsJsonArray(page.components, page);
            JsonArray linksJsonArray = makeLinksJsonArray(page, pages);

            // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
            if (page.getBannerImage() == null){
                page.setBannerImage("aaa");
            }
            JsonObject courseJsonObject = Json.createObjectBuilder()
                    .add(JSON_TITLE, ePortfolioToSave.getName())
                    .add("page_name", pageTitle)
                    .add("bannerImage", page.getBannerImage())
                    .add("layout", "css" + SLASH + page.getLayout() +".css")
                    .add("font", "css" + SLASH + page.getFont()+".css")
                    .add("style", "css" + SLASH + page.getColor()+".css")
                    .add("links", linksJsonArray)
                    .add("bannerText", slideShowTitle)
                    .add("content", slidesJsonArray)

                    .build();
                    Map<String, Object> properties = new HashMap<>(1);
            properties.put(JsonGenerator.PRETTY_PRINTING, true);

            JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
                StringWriter sw = new StringWriter();
            JsonWriter jsonWriter = writerFactory.createWriter(sw);
            jsonWriter.writeObject(courseJsonObject);
            jsonWriter.close();


            JsonWriter jsonFileWriter = Json.createWriter(os);
            jsonFileWriter.writeObject(courseJsonObject);
            String prettyPrinted = sw.toString();
            PrintWriter pw = new PrintWriter(jsonFilePath);
            pw.write(prettyPrinted);
            pw.close();
            System.out.println(prettyPrinted);
                // AND SAVE EVERYTHING AT ONCE
               // jsonWriter.writeObject(courseJsonObject);
        }
        
        String jsonFilePath = "./data/portfolios/" + SLASH  + slideShowTitle + SLASH +"loader" +JSON_EXT;
        OutputStream os = new FileOutputStream(jsonFilePath);
                
            //JsonWriter jsonWriter = Json.createWriter(os);  
            // BUILD THE SLIDES ARRAY
        ObservableList<String> pie;
            JsonArray locationJsonArray = makePagesJsonArray(loader, slideShowTitle);
            
            JsonObject courseJsonObject = Json.createObjectBuilder()
                    .add("pages", locationJsonArray)
                    .build();
                    Map<String, Object> properties = new HashMap<>(1);
            properties.put(JsonGenerator.PRETTY_PRINTING, true);

            JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
                StringWriter sw = new StringWriter();
            JsonWriter jsonWriter = writerFactory.createWriter(sw);
            jsonWriter.writeObject(courseJsonObject);
            jsonWriter.close();


            JsonWriter jsonFileWriter = Json.createWriter(os);
            jsonFileWriter.writeObject(courseJsonObject);
            String prettyPrinted = sw.toString();
            PrintWriter pw = new PrintWriter(jsonFilePath);
            pw.write(prettyPrinted);
            pw.close();
            System.out.println(prettyPrinted);
                // AND SAVE EVERYTHING AT ONCE
               // jsonWriter.writeObject(courseJsonObject);
            
    }
    
    
    
    
    
    
    
    
    
    private JsonArray makePagesJsonArray(ObservableList<String> loader, String title){
         JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (String s : loader) {
	    JsonObject jso = makeStringJsonObject(s, title);
	    jsb.add(jso);
        }
        
         
        
        JsonArray jA = jsb.build();
        return jA;        
    }
    
    
     private JsonObject makeStringJsonObject(String s, String t){
         
            JsonObject jso = Json.createObjectBuilder()
                    .add("location", "./data/portfolios/" + SLASH  + t + SLASH + s+ JSON_EXT)
                    
                    .build();
            return jso;
         
     }
    
    
     private JsonArray makeComponentsJsonArray(List<Component> components, Page page) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Component comp : components) {
	    JsonObject jso = makeComponentJsonObject(comp);
	    jsb.add(jso);
        }
        
         JsonObject jso = Json.createObjectBuilder()
                    .add("type", "f")
                    .add("text", page.getFooterText())
                    
                    .build();
        jsb.add(jso);
        
        JsonArray jA = jsb.build();
        return jA;        
       
    }
     
     
     private JsonArray makeLinksJsonArray(Page page, ObservableList<Page> pages){
         JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Page p : pages) {
	    JsonObject jso = makeLinkJsonObject(p, page);
	    jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;        
     }
     
     private JsonObject makeLinkJsonObject(Page p, Page page){
         if (p.equals(page) == false){
            JsonObject jso = Json.createObjectBuilder()
                    .add("link_name", p.getTitle())
                    .add("link_path", p.getTitle()+".html")
                    .add("current", "nav")
                    .build();
            return jso;
         }else{
             JsonObject jso = Json.createObjectBuilder()
                    .add("link_name", p.getTitle())
                    .add("link_path", p.getTitle()+".html")
                    .add("current", "current")
                    .build();
            return jso;
         }
     }
    
    private JsonObject makeComponentJsonObject(Component comp) {
        if (comp instanceof HeaderComponent){
            HeaderComponent hc = (HeaderComponent) comp;
            JsonObject jso = Json.createObjectBuilder()
		.add("type", "h")
                .add("text", hc.header)
		.build();
	return jso;
        }
         if (comp instanceof ParagraphComponent){
            ParagraphComponent pc = (ParagraphComponent) comp;
            JsonObject jso = Json.createObjectBuilder()
		.add("type", "p")
                .add("text", pc.paragraph)
                .add("font", pc.font)
		.build();
	return jso;
        }
        if (comp instanceof ListComponent){
            ListComponent lc = (ListComponent) comp;
            if (lc.items.size() == 1){
            JsonObject jso = Json.createObjectBuilder()
		.add("type", "l")
                .add("text", lc.items.size())
                .add("first", lc.items.get(0).item)
		.build();
            return jso;
            }
            if (lc.items.size() == 2){
            JsonObject jso = Json.createObjectBuilder()
		.add("type", "l")
                .add("text", lc.items.size())
                .add("first", lc.items.get(0).item)
                .add("second", lc.items.get(1).item)
		.build();
            return jso;
            }
            if (lc.items.size() == 3){
            JsonObject jso = Json.createObjectBuilder()
		.add("type", "l")
                .add("text", lc.items.size())
                .add("first", lc.items.get(0).item)
                .add("second", lc.items.get(1).item)
                .add("third", lc.items.get(2).item)
		.build();
            return jso;
            }
            if (lc.items.size() == 4){
            JsonObject jso = Json.createObjectBuilder()
		.add("type", "l")
                .add("text", lc.items.size())
                .add("first", lc.items.get(0).item)
                .add("second", lc.items.get(1).item)
                .add("third", lc.items.get(2).item)
                .add("fourth", lc.items.get(3).item)
		.build();
            return jso;
            }
            if (lc.items.size() == 5){
            JsonObject jso = Json.createObjectBuilder()
		.add("type", "l")
                .add("text", lc.items.size())
                .add("first", lc.items.get(0).item)
                .add("second", lc.items.get(1).item)
                .add("third", lc.items.get(2).item)
                .add("fourth", lc.items.get(3).item)
                .add("fifth", lc.items.get(4).item)
		.build();
            return jso;
            }
            if (lc.items.size() == 6){
            JsonObject jso = Json.createObjectBuilder()
		.add("type", "l")
                .add("text", lc.items.size())
                .add("first", lc.items.get(0).item)
                .add("second", lc.items.get(1).item)
                .add("third", lc.items.get(2).item)
                .add("fourth", lc.items.get(3).item)
                .add("fifth", lc.items.get(4).item)
                .add("sixth", lc.items.get(5).item)
		.build();
            return jso;
            }
            else{
            JsonObject jso = Json.createObjectBuilder()
		.add("type", "l")
                .add("text", lc.items.size())
                .add("first", lc.items.get(0).item)
                .add("second", lc.items.get(1).item)
                .add("third", lc.items.get(2).item)
                .add("fourth", lc.items.get(3).item)
                .add("fifth", lc.items.get(4).item)
                .add("sixth", lc.items.get(5).item)
                .add("seventh", lc.items.get(6).item)
               
                    
		.build();
            return jso;
            }
	
        }
         if (comp instanceof ImageComponent){
            ImageComponent ic = (ImageComponent) comp;
            JsonObject jso = Json.createObjectBuilder()
                
		.add("type", "i")
                .add("text", ic.path)
                .add("width", ic.width)
                .add("height", ic.height)
                .add("caption", ic.caption)
                .add("style", ic.floated)
		.build();
	return jso;
        }
         if (comp instanceof VideoComponent){
            VideoComponent vc = (VideoComponent) comp;
            JsonObject jso = Json.createObjectBuilder()
                
		.add("type", "v")
                .add("text", vc.path)
                .add("width", vc.width)
                .add("height", vc.height)
                .add("caption", vc.caption)               
		.build();
	return jso;
        }
        if (comp instanceof SlideShowComponent){
            SlideShowComponent ssc = (SlideShowComponent) comp;
            JsonObject jso = Json.createObjectBuilder()
                
		.add("type", "s")
                .add("text", "site")
                .add("width", "300")
                .add("height", "400")
              
		.build();
	return jso;
        }
        else{
         HeaderComponent hc = (HeaderComponent) comp;
            JsonObject jso = Json.createObjectBuilder()
		.add("type", "h")
                .add("text", "list")
		.build();
	return jso;
        }
    }
    
    
}
