/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.controller;
import eportfoliomaker.file.EPortfolioFileManager;
import eportfoliomaker.view.EPortfolioMakerView;
import java.io.IOException;
import eportfoliomaker.model.EPortfolioModel;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
/**
 *
 * @author Chris
 */
public class FileController {
    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;
    public static String showTitle = "a";
    // THE APP UI
    private EPortfolioMakerView ui;
    String title;
     public static String SLASH = "/";
    // THIS GUY KNOWS HOW TO READ AND WRITE EPORTFOLIO SHOW DATA
    private EPortfolioFileManager portfolioIO;
    
    
    
    public FileController(EPortfolioMakerView initUI, EPortfolioFileManager initSlideShowIO) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        portfolioIO = initSlideShowIO;
    }
    
    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewPortfolioRequest() {
       // try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                
                EPortfolioModel portfolio = ui.getPortfolio();
		portfolio.reset(1);
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ui.updateToolbarControls(saved);

                // TELL THE USER THE SLIDE SHOW HAS BEEN CREATED
                // @todo
            }
       // } catch (IOException ioe) {
          //  ErrorHandler eH = ui.getErrorHandler();
            // @todo provide error message
        //}
        markAsEdited();
    }
    
     /**
     * This method will save the current portfolio to a file. 
     */
     public boolean handleSaveAsPortfolioRequest(EPortfolioMakerView show) {
         //return promptToSave();
         if (promptToSave()){
             try{
                 show.getPortfolio().saveName = title;
         portfolioIO.saveSlideShow(show.getPortfolio());
         }catch(IOException e){System.out.println("There was an Error");}
             return true;
         }
         return false;
     }
    
     public void handleExportPortfolioRequest(EPortfolioMakerView show){
         if (promptToExport(show)){
             try{
                 show.getPortfolio().saveName = title;
                 
                 portfolioIO.exportPortfolio(show.getPortfolio());
        }catch(IOException e){System.out.println("There was an Error Exporting");}
             
         }
             
         
     }
     
     
     public void handleSavePortfolioRequest(EPortfolioMakerView show) {
         try{
         portfolioIO.saveSlideShow(show.getPortfolio());
         }catch(IOException e){System.out.println("There was an Error");}
     }
     
    /**
     * This method lets the user open a portfolio saved to a file. It will also
     * make sure data for the current portfolio is not lost.
     */
    public void handleLoadPortfolioRequest() {
       // try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
            }
       // } catch (IOException ioe) {
            //ErrorHandler eH = ui.getErrorHandler();
            //PropertiesManager props = PropertiesManager.getPropertiesManager();
            //String error =props.getProperty(ERROR_DATA_FILE_LOADING.toString());
            //eH.processError(ERROR_DATA_FILE_LOADING, ERROR_DATA_FILE_LOADING, PATH_SLIDE_SHOWS);
            //@todo provide error message
        //}
    }
    
    /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void handleExitRequest() {
       // try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave();
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
      // } catch (IOException ioe) {
           // ErrorHandler eH = ui.getErrorHandler();
            
	   // PropertiesManager props = PropertiesManager.getPropertiesManager();
           // String error =props.getProperty(ERROR_DATA_FILE_LOADING.toString());
           // eH.processError(ERROR_DATA_FILE_LOADING, NO_SAVE, PATH_SLIDE_SHOWS);
        //}
    }
    
    
    // HERE ARE OUR UI CONTROLS
    ScrollPane scrollPane;
    WebView webView;
    WebEngine webEngine;
    
    public void handleSwitchViewRequest(EPortfolioMakerView ui, int y){
        
        if (y == 0){
            handleExportPortfolioRequest(ui);
            
                    
        
            Stage stage = new Stage();
        webView = new WebView();
	scrollPane = new ScrollPane(webView);
	
	// GET THE URL
	String indexPath = "./data/exports/" +SLASH +  ui.getPortfolio().saveName +SLASH +ui.getPortfolio().selectedPage.getTitle() + ".html";
        System.out.println(indexPath);
	File indexFile = new File(indexPath);
        try{
	URL indexURL = indexFile.toURI().toURL();
	
	// SETUP THE WEB ENGINE AND LOAD THE URL
	webEngine = webView.getEngine();
	webEngine.load(indexURL.toExternalForm());
	webEngine.setJavaScriptEnabled(true);
	
	// SET THE WINDOW TITLE
	//this.setTitle(slides.getTitle());

	// NOW PUT STUFF IN THE STAGE'S SCENE
	Scene scene = new Scene(webView, 1100, 650);
	stage.setScene(scene);
        ui.getBorderPane().setCenter(webView);
	//stage.showAndWait();
        } catch(Exception e){}
            
            
            
            
             //Stage stage = new Stage();
            //Scene scene;
        //stage.setTitle(show.getTitle());
            //Screen screen = Screen.getPrimary();
	//Rectangle2D bounds = screen.getVisualBounds();
        //stage.setX(bounds.getMinX());
	//stage.setY(bounds.getMinY());
	//stage.setWidth(bounds.getWidth());
	//stage.setHeight(bounds.getHeight());
            //scene = new Scene(new Browser(),750,500, Color.web("#666970"));
        //stage.setScene(scene);
            //scene.getStylesheets().add("SlideShowMakerStyle.css");        
        //st/age.show();
            //ui.getBorderPane().setCenter(new Browser());
        }
        
        else{
            ui.getBorderPane().setCenter(ui.workspace);
        }
    }
    
     public class Browser extends Region {
 
    final WebView browser = new WebView();
    
    final WebEngine webEngine = browser.getEngine();
     
    public Browser()  {
        //apply the styles
        getStyleClass().add("tool_bar");
        // load the web page
        try{
         getClass().newInstance();
        } catch(InstantiationException e){
            System.out.println("");
        }catch( IllegalAccessException e){
            System.out.println("");
        }
        String path = "";
        Thread t = Thread.currentThread();
       
        
        
        
        
        
        
        
        
        ClassLoader c = t.getContextClassLoader();
        URL s =c.getResource("/src/exports/page1.html");
       
         //path = Thread.currentThread().getContextClassLoader().getResource("./src/exports/page1.html").toExternalForm();
        try{
            URL p = new File("/src/exports/page1.html").toURI().toURL();
            path = p.toExternalForm();
            System.out.println(path);
        }catch(Exception e){}
        
                //System.out.println("0.0" +path + "0.0");
       // webEngine.load(path);
        webEngine.loadContent(path);
        
        //add the web view to the scene
        getChildren().add(browser);
        webEngine.setJavaScriptEnabled(true);
    }
    private Node createSpacer() {
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        return spacer;
    }
 
    @Override protected void layoutChildren() {
        double w = getWidth();
        double h = getHeight();
        layoutInArea(browser,0,0,w,h,0, HPos.CENTER, VPos.CENTER);
    }
 
    @Override protected double computePrefWidth(double height) {
        return 750;
    }
 
    @Override protected double computePrefHeight(double width) {
        return 500;
    }
    }
    
    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is retuned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    boolean saveWork;
    boolean cancel =false;
    boolean dont = false;
     private boolean promptToSave() {//throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        
        Stage saveStage = new Stage();
        Stage langStage = new Stage();
        Button btYES = new Button("      " + "Save" + "       ");
        Button btNO = new Button("   " +"Don't Save"+ "    ");
        Button btCANCEL = new Button("     " +"Cancel"+ "      " );
        Label langLabel = new Label("      SAVE AS");
        TextField titleText = new TextField();
        // set allignment
        GridPane p = new GridPane();
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        GridPane pane1 = new GridPane();
        pane1.setAlignment(Pos.CENTER);
        GridPane pane2 = new GridPane();
        pane2.setAlignment(Pos.CENTER);
        GridPane pane3 = new GridPane();
        pane3.setAlignment(Pos.CENTER);
        p.add(langLabel, 10, 0);
        p.add(new Label(""), 10, 15);
        p.add(titleText, 10, 25);
        p.add(new Label(""), 10, 35);
        p.add(btYES, 10, 45);
         p.add(new Label(""), 10, 55);
        p.add(btNO, 10, 65);
         p.add(new Label(""), 10, 75);
        p.add(btCANCEL, 10, 85);
        
        p.getStyleClass().add("btYES");
        Scene scene = new Scene(p, 350, 250);
        scene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
        btYES.getStyleClass().add("btYES");
        saveStage.setTitle("SAVE");
        saveStage.setScene(scene);
        
        
        ///if yes is selected
        btYES.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            saveWork = true;
            title = titleText.getText();
            saveStage.close();
        }
        });
        
        
        ///if no is selected
        btNO.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            saveWork = false;
            saveStage.close();
            dont = true;
        }
        });
        
        ///if cancel is selected
        btCANCEL.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            cancel = true;
            saveStage.close();
        }
        });
        
        
        saveStage.showAndWait();
        
        
        
        
        
         // @todo change this to prompt
        if (dont ){
            return true;
        }
        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            //SlideShowModel slideShow = ui.getSlideShow();
            //slideShowIO.saveSlideShow(slideShow);
            saved = true;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        if (cancel) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }
    
     boolean exportWork = false;
    private boolean promptToExport(EPortfolioMakerView show) {//throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        
        Stage saveStage = new Stage();
        Stage langStage = new Stage();
        Button btYES = new Button("     " + "Export" + "      ");
        Button btNO = new Button("  " +"Don't Export"+ "   ");
        Button btCANCEL = new Button("     " +"Cancel"+ "      " );
        Label langLabel = new Label("      Export");
        TextField titleText = new TextField();
        // set allignment
        GridPane p = new GridPane();
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        GridPane pane1 = new GridPane();
        pane1.setAlignment(Pos.CENTER);
        GridPane pane2 = new GridPane();
        pane2.setAlignment(Pos.CENTER);
        GridPane pane3 = new GridPane();
        pane3.setAlignment(Pos.CENTER);
        p.add(langLabel, 10, 0);
        p.add(new Label(""), 10, 15);
        p.add(titleText, 10, 25);
        p.add(new Label(""), 10, 35);
        p.add(btYES, 10, 45);
         p.add(new Label(""), 10, 55);
        p.add(btNO, 10, 65);
        // p.add(new Label(""), 10, 75);
        //p.add(btCANCEL, 10, 85);
        
        p.getStyleClass().add("btYES");
        Scene scene = new Scene(p, 350, 250);
        scene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
        btYES.getStyleClass().add("btYES");
        saveStage.setTitle("SAVE");
        saveStage.setScene(scene);
        
        
        ///if yes is selected
        btYES.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            exportWork = true;
            title = titleText.getText();
            show.getPortfolio().saveName=title;
            saveStage.close();
        }
        });
        
        
        ///if no is selected
        btNO.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            exportWork = false;
            saveStage.close();
            dont = true;
        }
        });
        
        ///if cancel is selected
        btCANCEL.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            cancel = true;
            saveStage.close();
        }
        });
        
        
        saveStage.showAndWait();
        
        
        
        
        
         // @todo change this to prompt
        if (dont ){
            return true;
        }
        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (exportWork) {
            //SlideShowModel slideShow = ui.getSlideShow();
            //slideShowIO.saveSlideShow(slideShow);
            saved = true;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        if (exportWork == false) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }
    
    
     /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File("./data/portfolios/"));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

         if (selectedFile != null) {
            try {
		EPortfolioModel slideShowToLoad = ui.getPortfolio();
                
                portfolioIO.loadPortfolio(slideShowToLoad, selectedFile.getAbsolutePath());
                System.out.println("Make it out?");
                String path = selectedFile.getAbsolutePath();
                //title=title.substring(65, title.length()-5);
               // int lastSlash =path.lastIndexOf("\\");
                //String title = path.substring(lastSlash +1, path.lastIndexOf("."));
                
               
                ui.reloadSlideShowPane(slideShowToLoad);
                saved = true;
                ui.updateToolbarControls(saved);
                
            } catch (Exception e) {
                System.out.println("Error here?");
                // @todo
            }
        }
        
    }
    
    
    
    public void markAsEdited() {
        saved = false;
        ui.updateToolbarControls(saved);
    }
    
    
      public void markFileAsNotSaved() {
        saved = false;
    }
    
    
    
    
    
    
    
   
    
    
    
    
    
    
    
    
    
    
}
