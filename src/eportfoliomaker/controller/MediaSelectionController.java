/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.controller;

import eportfoliomaker.model.Page;
import eportfoliomaker.view.PageEditView;
import java.io.File;
import javafx.stage.FileChooser;

/**
 *
 * @author Chris
 */
public class MediaSelectionController {
    
    
    
    
    
     public void processSelectImage(Page pageToEdit, PageEditView view) {
	FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File("./images/slide_show_Images"));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
            String total = path+fileName;
	    //pageToEdit.setBannerImage(total);
	    //view.bannerButton.setText(fileName);
            view.currentImagePath = (total);
            view.currentImageName = (fileName);
	}	    
	else {
	    // ErrorHandler eH = view.getMaker().getErrorHandler();
	   // PropertiesManager props = PropertiesManager.getPropertiesManager();
            //String error =props.getProperty(ERROR_DATA_FILE_LOADING.toString());
            //eH.processError(ERROR_DATA_FILE_LOADING, NO_IMAGE, PATH_SLIDE_SHOWS);
	}
    }
     
     
     
     
     
     
     
     
     public void processSelectVideo(Page pageToEdit, PageEditView view) {
	FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File("./videos"));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter mp4Filter = new FileChooser.ExtensionFilter("MP4 files (*.mp4)", "*.MP4");
	
	imageFileChooser.getExtensionFilters().addAll(mp4Filter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
            String total = path + fileName;
             view.currentVideoPath = (total);
            view.currentVideoName = (fileName);
	    //slideToEdit.setImage(path, fileName);
	    //view.updateSlideImage();
	}	    
	else {
	    // ErrorHandler eH = view.getMaker().getErrorHandler();
	   // PropertiesManager props = PropertiesManager.getPropertiesManager();
            //String error =props.getProperty(ERROR_DATA_FILE_LOADING.toString());
            //eH.processError(ERROR_DATA_FILE_LOADING, NO_IMAGE, PATH_SLIDE_SHOWS);
	}
    }
}
