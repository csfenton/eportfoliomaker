/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.controller;
import eportfoliomaker.model.EPortfolioModel;
import eportfoliomaker.model.Page;
import eportfoliomaker.view.EPortfolioMakerView;



/**
 *
 * @author Chris
 */
public class PortfolioEditController {
    // APP UI
    private EPortfolioMakerView ui;
    
    /**
     * This constructor keeps the UI for later.
     */
    public PortfolioEditController(EPortfolioMakerView initUI) {
	ui = initUI;
    }
    
    
    
      /**
     * Provides a response for when the user wishes to add a new
     * page to the portfolio .
     */
    public void processAddPageRequest() {
	EPortfolioModel portfolio = ui.getPortfolio();
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	portfolio.addPage();
        ui.updateToolbarControls(false);
    }
    
    public void processRemovePageRequest(Page page){
        if (page !=null){
            EPortfolioModel portfolio = ui.getPortfolio();
            portfolio.removePage(page);
            ui.updateToolbarControls(false);
        }
    }
    
    
    
}
