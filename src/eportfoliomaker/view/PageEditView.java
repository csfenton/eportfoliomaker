/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.view;

import eportfoliomaker.component.Component;
import eportfoliomaker.component.HeaderComponent;
import eportfoliomaker.component.ImageComponent;
import eportfoliomaker.component.ListComponent;
import eportfoliomaker.component.ListItem;
import eportfoliomaker.component.ParagraphComponent;
import eportfoliomaker.component.Slide;
import eportfoliomaker.component.SlideShowComponent;
import eportfoliomaker.component.SlideShowMakerView;
import eportfoliomaker.component.VideoComponent;
import eportfoliomaker.controller.MediaSelectionController;
import eportfoliomaker.model.Page;
import java.io.File;
import java.net.URL;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author Chris
 */
public class PageEditView extends HBox{
    // SLIDE THIS COMPONENT EDITS
    Page page;
    Tab tab;
    
    //Slideshow Maker Viewto Contain list of slides
    EPortfolioMakerView maker;
    
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    ScrollPane scroller;
    Pane pageSpace;
    Label captionLabel;
    TextField captionTextField = new TextField();
    String caption;
    int componentHeight = 515;
    // PROVIDES RESPONSES FOR Media SELECTION
    MediaSelectionController mediaController;
    public String currentImagePath;
    public String currentImageName;
    public String currentVideoPath;
    public String currentVideoName;
    //adding list components
    int y =1;
    int start;
    int end;
    
    //EVERYTHING IN THE PAGE ITSELF
    public Button bannerButton = new Button("  Choose \nBanner Image");
    public TextField pageNameText = new TextField();
    public TextField studentNameText = new TextField();
    public ObservableList<String> layouts = FXCollections.observableArrayList("Layout 1", "Layout 2", "Layout 3", "Layout 4", "Layout 5");
    public final ComboBox layoutBox = new ComboBox(layouts);
    public ObservableList<String> colors = FXCollections.observableArrayList("219 Theme", "Aquamarine Theme", "Wild Jungle Theme", "Dark Slate Theme", "Pre-School Theme");
    public final ComboBox colorBox = new ComboBox(colors);
    public ObservableList<String> fonts = FXCollections.observableArrayList("Georgia", "Blooming Flowers", "Arial", "Block Palin", "Eccentric");
    public final ComboBox fontBox = new ComboBox(fonts);
    public TextField footerText = new TextField();
    public Button headerButton = new Button("Add Header");
    public Button paragraphButton = new Button("Add Paragraph");
    public Button listButton = new Button("Add List");
    public Button imageButton = new Button("Add Image");
    public Button videoButton = new Button("Add Video");
    public Button slideShowButton = new Button("Add Slide Show");
    public Button removeComponentButton = new Button("Remove Selected Component");
     public ObservableList<Slide> slides = FXCollections.observableArrayList();
     public SlideShowMakerView slideShowMakerView;
    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     */
    public PageEditView(Page initPage, EPortfolioMakerView ssmv) {
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	this.getStyleClass().add("page_edit_view");
	maker = ssmv;
	// KEEP THE SLIDE FOR LATER
	page = initPage;
	page.setEditor(this);
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	//updateSlideImage();

	// SETUP THE CAPTION CONTROLS
        
	pageSpace = new Pane();
        pageSpace.setMinWidth(1000);
	scroller = new ScrollPane(pageSpace);
        
        //SETTING UP THE LAYOUT FOR THE PAGE SPACE//
         
        
        //PAGE TITLE
	Label pageNameLabel = new Label("Page Title"); 
        
        pageNameText.setLayoutY(15);
        pageNameText.setMinWidth(150);
        
        
        pageNameText.setOnKeyPressed(e -> {
            tab.setText(pageNameText.getText());
            initPage.setTitle(pageNameText.getText());
	});
        
        //STUDENT NAME
        Label studentNameLabel = new Label("Student Name");
        studentNameLabel.setLayoutY(40);
        
        studentNameText.setLayoutY(55);
        studentNameText.setMinWidth(150);
        
        studentNameText.setOnKeyPressed(e -> {
            //tab.setText(pageNameText.getText());
            initPage.setName(studentNameText.getText());
	});
        
        
        
        //LAYOUT, COLOR AND FONT SELECTIONS
        Label layoutLabel = new Label ("Layout");
        layoutLabel.setLayoutY(95);
            
                //layoutBox.setPromptText("Layout");
                layoutBox.setStyle("-fx-text-align: center");
                
                layoutBox.setLayoutY(110);
                layoutBox.setMinWidth(150);
                
                
       Label colorLabel = new Label ("Color Scheme");
        colorLabel.setLayoutY(95);
        colorLabel.setLayoutX(190);
            
                //layoutBox.setPromptText("Layout");
                layoutBox.setStyle("-fx-text-align: center");
                
                colorBox.setLayoutY(110);
                colorBox.setLayoutX(190);
                colorBox.setMinWidth(150);
                         
                
       Label fontLabel = new Label ("Font Scheme");
        fontLabel.setLayoutY(95);
        fontLabel.setLayoutX(400);
            
                //layoutBox.setPromptText("Layout");
                fontBox.setStyle("-fx-text-align: center");
                
                fontBox.setLayoutY(110);
                fontBox.setLayoutX(400);
                fontBox.setMinWidth(150);
        
        
        //BANNER IMAGE 
       
        bannerButton.setMinWidth(150);
        bannerButton.setMaxWidth(150);
        bannerButton.setLayoutY(200);
        
        //FOOTER TEXT
        Label footerLabel = new Label("Enter Footer Text");
        //footerLabel.setMinWidth(300);
        footerLabel.setLayoutY(185);
        footerLabel.setLayoutX(200);
        
        
        footerText.setMinWidth(375);
        footerText.setLayoutY(200);
        footerText.setMinHeight(40);
        footerText.setLayoutX(200);
        
        footerText.setOnKeyPressed(e -> {
            
            initPage.setFooterText(footerText.getText());
	});
        
        //ADD HEADER 
       
        headerButton.setLayoutY(300);
        headerButton.setMinWidth(150);
        headerButton.setMinHeight(30);
        
     
        
        //ADD PARAGRAPH
        
        paragraphButton.setLayoutY(300);
        paragraphButton.setMinWidth(150);
       paragraphButton.setMinHeight(30);
        paragraphButton.setLayoutX(190);
        
        //ADD LIST
        
       listButton.setLayoutY(300);
        listButton.setMinWidth(150);
        listButton.setMinHeight(30);
        listButton.setLayoutX(400);
        
        //ADD IMAGE
        
        imageButton.setLayoutY(375);
        imageButton.setMinWidth(150);
        imageButton.setMinHeight(30);
        
        //ADD VIDEO
        
        videoButton.setLayoutY(375);
        videoButton.setMinWidth(150);
        videoButton.setMinHeight(30);
        videoButton.setLayoutX(190);
        
        //ADD SLIDESHOW
        
        slideShowButton.setLayoutY(375);
        slideShowButton.setMinWidth(150);
        slideShowButton.setMinHeight(30);
        slideShowButton.setLayoutX(400);
        
        
        //REMOVE COMPONENT
       
        removeComponentButton.setMinWidth(150);
        removeComponentButton.setMinHeight(30);
        removeComponentButton.setLayoutY(475);
        
           
        
        
        //Header ComponentButton
         Button headerComponentButton = new Button("Header Component");
        headerComponentButton.setMinWidth(200);
        headerComponentButton.setMinHeight(30);
        headerComponentButton.setLayoutY(515);
        headerComponentButton.setStyle("-fx-color: #FFFFFF");        
        
        
	pageSpace.getChildren().add(pageNameLabel);
	pageSpace.getChildren().add(pageNameText);
        pageSpace.getChildren().add(studentNameLabel);
	pageSpace.getChildren().add(studentNameText);
        pageSpace.getChildren().add(layoutLabel);
        pageSpace.getChildren().add(layoutBox);
        pageSpace.getChildren().add(colorLabel);
        pageSpace.getChildren().add(colorBox);
        pageSpace.getChildren().add(fontLabel);
        pageSpace.getChildren().add(fontBox);
        pageSpace.getChildren().add(bannerButton);
        pageSpace.getChildren().add(footerLabel);
        pageSpace.getChildren().add(footerText);
        pageSpace.getChildren().add(headerButton);
        pageSpace.getChildren().add(paragraphButton);
        pageSpace.getChildren().add(listButton);
        pageSpace.getChildren().add(imageButton);
        pageSpace.getChildren().add(videoButton);
        pageSpace.getChildren().add(slideShowButton);
        pageSpace.getChildren().add(removeComponentButton);
        //pageSpace.getChildren().add(headerComponentButton);
        
        
        //HANDLERS FOR THE TEXT FIELDS
        
        
        
      
        
        
        //HANDLERS FOR ALL THE PAGE EDIT BUTTONS
        mediaController = new MediaSelectionController();
        
        //SELECT BANNER IMAGE
        bannerButton.setOnMousePressed(e -> {
	    mediaController.processSelectImage(page, this);
            page.setSelected(true);
            maker.getPortfolio().selected(page);
            page.setSelected(true);
            maker.getPortfolio();
            bannerButton.setText(currentImageName +" \n ");
            page.setBannerName(currentImageName);
            page.setBannerImage(currentImagePath);
            //captionVBox.setOpacity(.25);
            //imageSelectionView.setOpacity(.25);
	});
        
        
        //REMOVES COMPONENT
        removeComponentButton.setOnAction(e->{
            //System.out.println(page.getSelectedComponent().name);
            if (page.getSelectedComponent() != null){
               
                page.components.remove(page.getSelectedComponent());
                pageSpace.getChildren().remove(page.getSelectedComponent().compButton);
                page.setSelectedComponent(null);
           }
        });
            
        
        //SELECT FONT
        fontBox.setOnAction(e->{
           initPage.setFont(fontBox.getValue().toString());
        });
        
         colorBox.setOnAction(e->{
           initPage.setColor(colorBox.getValue().toString());
        });
         
          layoutBox.setOnAction(e->{
           initPage.setLayout(layoutBox.getValue().toString());
           
        });
        
        //ADD HEADER
        headerButton.setOnMousePressed(e -> {
            headerPrompt(initPage);
            
	});
        
        headerComponentButton.setOnMousePressed(e -> {
            headerPrompt(initPage);
	});
        
        
        //ADD PARAGRAPH
        paragraphButton.setOnMousePressed(e -> {
            paragraphPrompt(initPage);
	});
        
        //ADD VIDEO
        videoButton.setOnMousePressed(e -> {
            videoPrompt(initPage);
	});
        
         //ADD List
        listButton.setOnMousePressed(e -> {
            listPrompt(initPage);
	});
        
        
        
      //ADD IMAGE
             imageButton.setOnMousePressed(e -> {
            imagePrompt(initPage);
	});
        
             
             //ADD SLIDE SHOW
             
        slideShowButton.setOnMousePressed(e ->{
            slideShowPrompt(initPage);
        });
        
        
        
        
        

	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(imageSelectionView);
	getChildren().add(pageSpace);

	// SETUP THE EVENT HANDLERS
	//imageController = new ImageSelectionController();
	
        
        pageSpace.setOnMousePressed(e -> {
	    //imageController.processSelectImage(slide, this);
            page.setSelected(true);
            maker.getPortfolio().selected(page);
            page.setSelected(true);
            maker.getPortfolio();
            //captionVBox.setOpacity(.25);
            //imageSelectionView.setOpacity(.25);
	});
        captionTextField.setOnKeyPressed(e ->{
            ssmv.updateToolbarControls(false);
            
        });
       //ssmv.gettf().setOnKeyPressed(e ->{
         //   ssmv.updateToolbarControls(false);
            
        //});
    }
    
    ////////////////////////////
    //ADDING COMPONENT BUTTONS//
    ////////////////////////////
    public void addHeaderComponent(HeaderComponent c){
        Button cb = c.compButton;
        cb.setMinWidth(200);
        cb.setMinHeight(30);
        cb.setLayoutY(componentHeight);
        cb.setStyle("-fx-color: #FFFFFF"); 
        componentHeight +=45;
        page.setSelectedComponent(c);
        cb.setOnAction(e->{
            editHeader(c);
            page.setSelectedComponent(c);
        });
        
        pageSpace.getChildren().add(cb);
    }
    
    
    
    
    
      public void addParagraphComponent(ParagraphComponent c){
        Button cb = c.compButton;
        cb.setMinWidth(200);
        cb.setMinHeight(30);
        cb.setLayoutY(componentHeight);
        cb.setStyle("-fx-color: #FFFFFF"); 
        componentHeight +=45;
        cb.setOnAction(e->{
            editParagraph(c);
            page.setSelectedComponent(c);
        });
        
        pageSpace.getChildren().add(cb);
    }
     public void addImageComponent(ImageComponent c){
        Button cb = c.compButton;
        cb.setMinWidth(200);
        cb.setMinHeight(30);
        cb.setLayoutY(componentHeight);
        cb.setStyle("-fx-color: #FFFFFF"); 
        componentHeight +=45;
        cb.setOnAction(e->{
            editImage(c);
            page.setSelectedComponent(c);
        });
        
        pageSpace.getChildren().add(cb);
    }
    
     public void addVideoComponent(VideoComponent c){
        Button cb = c.compButton;
        cb.setMinWidth(200);
        cb.setMinHeight(30);
        cb.setLayoutY(componentHeight);
        cb.setStyle("-fx-color: #FFFFFF"); 
        componentHeight +=45;
        cb.setOnAction(e->{
            editVideo(c);
            page.setSelectedComponent(c);
        });
        
        pageSpace.getChildren().add(cb);
    }
    
    public void addListComponent(ListComponent c){
        Button cb = c.compButton;
        cb.setMinWidth(200);
        cb.setMinHeight(30);
        cb.setLayoutY(componentHeight);
        cb.setStyle("-fx-color: #FFFFFF"); 
        componentHeight +=45;
        cb.setOnAction(e->{
            editList(c);
            page.setSelectedComponent(c);
        });
        
        pageSpace.getChildren().add(cb);
    }
    
    public void addSlideShowComponent(SlideShowComponent c){
        Button cb = c.compButton;
        cb.setMinWidth(200);
        cb.setMinHeight(30);
        cb.setLayoutY(componentHeight);
        cb.setStyle("-fx-color: #FFFFFF"); 
        componentHeight +=45;
        cb.setOnAction(e->{
            editSlideShow(c);
            page.setSelectedComponent(c);
        });
        
        pageSpace.getChildren().add(cb);
    }
    
    ////////////////////////////////////////
    //ALL THE PROMPTS TO CREATE COMPONENTS//
    ////////////////////////////////////////
    public void headerPrompt(Page page){
        Stage headerStage = new Stage();
        
        Label headerLabel = new Label("Enter a Header");
        TextField headerText = new TextField();
        headerText.setMinWidth(300);
        Button ok = new Button("Okay");
       Pane p = new Pane();
         p.getChildren().add(headerLabel);
       // p.add(new Label(""), 10, 15);
        p.getChildren().add(headerText);
        p.getChildren().add(ok);
        // p.add(new Label(""), 10, 55);
        
        headerLabel.setLayoutY(50);
        headerLabel.setLayoutX(120);
        
         headerText.setLayoutY(70);
        headerText.setLayoutX(25);
        
         ok.setLayoutY(100);
        ok.setLayoutX(145);
        
        
        
         p.getStyleClass().add("btYES");
        Scene scene = new Scene(p, 350, 180);
        scene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
      
        headerStage.setTitle("Set a Header");
        headerStage.setScene(scene);
        
        
        
        ok.setOnAction(e -> {
            //Component comp = new Component();
            HeaderComponent header = new HeaderComponent(headerText.getText());
            page.components.add(header);
            page.setHeader(headerText.getText());
            page.setSelectedComponent(header);
            addHeaderComponent(header);
             headerStage.close();
        });
        
        
        headerStage.showAndWait();
        
    }
    
    
    public void editHeader(HeaderComponent hc){
         Stage headerStage = new Stage();
        
        Label headerLabel = new Label("Enter a Header");
        TextField headerText = new TextField();
        headerText.setText(hc.name);
        headerText.setMinWidth(300);
        Button ok = new Button("Okay");
       Pane p = new Pane();
         p.getChildren().add(headerLabel);
       // p.add(new Label(""), 10, 15);
        p.getChildren().add(headerText);
        p.getChildren().add(ok);
        // p.add(new Label(""), 10, 55);
        
        headerLabel.setLayoutY(50);
        headerLabel.setLayoutX(120);
        
         headerText.setLayoutY(70);
        headerText.setLayoutX(25);
        
         ok.setLayoutY(100);
        ok.setLayoutX(145);
        
        
        
         p.getStyleClass().add("btYES");
        Scene scene = new Scene(p, 350, 180);
        scene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
      
        headerStage.setTitle("Set a Header");
        headerStage.setScene(scene);
        
        
        
        ok.setOnAction(e -> {
            //Component comp = new Component();
            if (headerText.getText() != null){
             hc.name = headerText.getText();
                page.setHeader(headerText.getText());
                
            }else{
                hc.name =" ";
                
            }
            //addHeaderComponent(header);
            page.setSelectedComponent(hc);
             headerStage.close();
        });
        
        
        headerStage.showAndWait();
    }
    
    
    public void paragraphPrompt(Page page){
         Stage paragraphStage = new Stage();
        
        Label paragraphLabel = new Label("Write a paragraph");
        Label fontLabel = new Label("Choose a font");
        Label hyperLabel = new Label("Optional Hyperlink URL");
        ObservableList<String> fonts = FXCollections.observableArrayList("Georgia", "Blooming Flowers", "Arial", "Block Palin", "Eccentric");
                final ComboBox fontBox = new ComboBox(fonts);
                //layoutBox.setPromptText("Layout");
                fontBox.setStyle("-fx-text-align: center");
        
        
        paragraphLabel.setStyle("-fx-font-size: 12pt");
        fontLabel.setStyle("-fx-font-size: 12pt");
        hyperLabel.setStyle("-fx-font-size: 12pt");
        TextArea paragraphText = new TextArea();
        TextField hyperText = new TextField();
        paragraphText.setWrapText(true);
       paragraphText.setMinWidth(300);
       paragraphText.setMinHeight(300);
        
        Button ok = new Button("Finish Paragraph");
        ok.setMinHeight(20);
        
        Button linker = new Button("Make selected text hypertext");
        linker.setMinHeight(20);
       Pane p = new Pane();
         p.getChildren().add(paragraphLabel);
        p.getChildren().add(paragraphText);
        p.getChildren().add(ok);
       p.getChildren().add(fontLabel);
       p.getChildren().add(fontBox);
       p.getChildren().add(hyperLabel);
       p.getChildren().add(hyperText); 
       p.getChildren().add(linker);
       
       fontLabel.setLayoutY(50);
       fontLabel.setLayoutX(400);
        fontBox.setLayoutY(70);
       fontBox.setLayoutX(400);
        
       hyperLabel.setLayoutY(220);
       hyperLabel.setLayoutX(400);
       
       hyperText.setLayoutY(240);
       hyperText.setLayoutX(400);
       hyperText.setMinWidth(220);
       
       linker.setLayoutY(270);
       linker.setLayoutX(400);
       linker.setMinWidth(220);
       
        paragraphLabel.setLayoutY(50);
        paragraphLabel.setLayoutX(15);
        
        paragraphText.setLayoutY(70);
        paragraphText.setLayoutX(15);
        
         ok.setLayoutY(390);
        ok.setLayoutX(300);
        
        linker.setOnAction(w->{
               String a = paragraphText.getSelectedText();
               start = paragraphText.getText().indexOf(a);
               end = start + a.length();
           });
        
        ok.setOnAction(e->{
            
           String font =  fontBox.getValue().toString();
           String link = hyperText.getText();
           String paragraph = paragraphText.getText();
           ParagraphComponent para = new ParagraphComponent(paragraph, link, font, start, end);
            page.components.add(para);
            page.setSelectedComponent(para);
            addParagraphComponent(para);
            paragraphStage.close();
        });
        
         p.getStyleClass().add("btYES");
        Scene scene = new Scene(p, 700, 420);
        scene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
      
        paragraphStage.setTitle("Write a paragraph");
        paragraphStage.setScene(scene);
       
        paragraphStage.showAndWait();
    }
    
    public void editParagraph(ParagraphComponent pc){
         Stage paragraphStage = new Stage();
        
        Label paragraphLabel = new Label("Write a paragraph");
        Label fontLabel = new Label("Choose a font");
        Label hyperLabel = new Label("Optional Hyperlink URL");
        ObservableList<String> fonts = FXCollections.observableArrayList("Georgia", "Blooming Flowers", "Arial", "Block Palin", "Eccentric");
                final ComboBox fontBox = new ComboBox(fonts);
                //layoutBox.setPromptText("Layout");
                fontBox.setStyle("-fx-text-align: center");
        fontBox.setValue(pc.font);
        
        paragraphLabel.setStyle("-fx-font-size: 12pt");
        fontLabel.setStyle("-fx-font-size: 12pt");
        hyperLabel.setStyle("-fx-font-size: 12pt");
        TextArea paragraphText = new TextArea(pc.paragraph);
        
        TextField hyperText = new TextField(pc.link);
        paragraphText.setWrapText(true);
       paragraphText.setMinWidth(300);
       paragraphText.setMinHeight(300);
        
        Button ok = new Button("Finish Paragraph");
        ok.setMinHeight(20);
        
        Button linker = new Button("Make selected text hypertext");
        linker.setMinHeight(20);
       Pane p = new Pane();
         p.getChildren().add(paragraphLabel);
        p.getChildren().add(paragraphText);
        p.getChildren().add(ok);
       p.getChildren().add(fontLabel);
       p.getChildren().add(fontBox);
       p.getChildren().add(hyperLabel);
       p.getChildren().add(hyperText); 
       p.getChildren().add(linker);
       
       fontLabel.setLayoutY(50);
       fontLabel.setLayoutX(400);
        fontBox.setLayoutY(70);
       fontBox.setLayoutX(400);
        
       hyperLabel.setLayoutY(220);
       hyperLabel.setLayoutX(400);
       
       hyperText.setLayoutY(240);
       hyperText.setLayoutX(400);
       hyperText.setMinWidth(220);
       
       linker.setLayoutY(270);
       linker.setLayoutX(400);
       linker.setMinWidth(220);
       
        paragraphLabel.setLayoutY(50);
        paragraphLabel.setLayoutX(15);
        
        paragraphText.setLayoutY(70);
        paragraphText.setLayoutX(15);
        
         ok.setLayoutY(390);
        ok.setLayoutX(300);
        
        linker.setOnAction(w->{
               String a = paragraphText.getSelectedText();
               start = paragraphText.getText().indexOf(a);
               end = start + a.length();
           });
        
        ok.setOnAction(e->{
            
           String font =  fontBox.getValue().toString();
           String link = hyperText.getText();
           String paragraph = paragraphText.getText();
           //ParagraphComponent para = new ParagraphComponent(paragraph, link, font, start, end);
           pc.font= paragraph;
           pc.link = link;
           pc.font = font;
           pc.linkStart= start;
           pc.linkEnd = end;
            //page.components.add(para);
            page.setSelectedComponent(pc);
            //addParagraphComponent(para);
            paragraphStage.close();
        });
        
         p.getStyleClass().add("btYES");
        Scene scene = new Scene(p, 700, 420);
        scene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
      
        paragraphStage.setTitle("Write a paragraph");
        paragraphStage.setScene(scene);
       
        paragraphStage.showAndWait();
    }
    
    public void listPrompt(Page page){
        Stage listStage = new Stage();
        BorderPane bp = new BorderPane();
        GridPane listPane = new GridPane();
        ScrollPane spane = new ScrollPane(listPane);
        //spane.setMaxHeight(225);
        spane.setMinHeight(255);
        Pane okPane = new Pane();
        Button okButton = new Button("Okay");
        
        okButton.setLayoutX(110);
        okPane.getChildren().add(okButton);
        
        bp.setTop(spane);
        bp.setBottom(okPane);
        
        listPane.setHgap(10);
        listPane.setVgap(10);
        Scene scene = new Scene(bp, 255, 300);
        scene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
        listPane.getStyleClass().add("btYES");
        listPane.setStyle("-fx-spacing: 50px");
        
        
        listStage.setTitle("Create a list");
        listStage.setScene(scene);
        
        Label listLabel = new Label("List          ");
        TextField listField = new TextField();
        Button add = new Button("Add New List Item");
        Button remove = new Button("Remove List Item ");
        
        listPane.add(listLabel, 0, 0);
        //listPane.add(listField, 0, 1);
        listPane.add(add, 1, 0);
        //listPane.add(remove, 1, 1);
        // listPane.add(new TextField(), 0, 2);
        //listPane.add(new Button("Remove List Item "), 1, 2);
        
       ObservableList<ListItem> items = FXCollections.observableArrayList();
        
       add.setOnAction(e ->{
           ListItem li = new ListItem();
           listPane.add(li.listText, 0, y);
           listPane.add(li.removeButton, 1, y);
           items.add(li);
           li.removeButton.setOnAction(w->{
               items.remove(li);
               listPane.getChildren().remove(li.listText);
               listPane.getChildren().remove(li.removeButton);
               
           });
           
           y++;
       });
       
       
       okButton.setOnAction(e->{
           for (ListItem li: items){
               li.setItem(li.listText.getText());
           }
           ListComponent lc = new ListComponent(items);
           page.components.add(lc);
           page.setSelectedComponent(lc);
           addListComponent(lc);
           y=0;
           listStage.close();
           
           
             
       });
       
        listStage.showAndWait();
    }
    
    public void editList(ListComponent lc){
        Stage listStage = new Stage();
        BorderPane bp = new BorderPane();
        GridPane listPane = new GridPane();
        ScrollPane spane = new ScrollPane(listPane);
        //spane.setMaxHeight(225);
        spane.setMinHeight(255);
        Pane okPane = new Pane();
        Button okButton = new Button("Okay");
        
        okButton.setLayoutX(110);
        okPane.getChildren().add(okButton);
        
        bp.setTop(spane);
        bp.setBottom(okPane);
        
        listPane.setHgap(10);
        listPane.setVgap(10);
        Scene scene = new Scene(bp, 255, 300);
        scene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
        listPane.getStyleClass().add("btYES");
        listPane.setStyle("-fx-spacing: 50px");
        
        
        listStage.setTitle("Create a list");
        listStage.setScene(scene);
        
        Label listLabel = new Label("List          ");
        TextField listField = new TextField();
        Button add = new Button("Add New List Item");
        Button remove = new Button("Remove List Item ");
        
        listPane.add(listLabel, 0, 0);
        //listPane.add(listField, 0, 1);
        listPane.add(add, 1, 0);
        //listPane.add(remove, 1, 1);
        // listPane.add(new TextField(), 0, 2);
        //listPane.add(new Button("Remove List Item "), 1, 2);
        
       ObservableList<ListItem> items = lc.items;
       for (ListItem li: items){
            listPane.add(li.listText, 0, y);
            listPane.add(li.removeButton, 1, y);
            y++;
            li.removeButton.setOnAction(w->{
               items.remove(li);
               listPane.getChildren().remove(li.listText);
               listPane.getChildren().remove(li.removeButton);
               
           });
           
           
       }
        
       add.setOnAction(e ->{
           ListItem li = new ListItem();
           listPane.add(li.listText, 0, y);
           listPane.add(li.removeButton, 1, y);
           items.add(li);
           li.removeButton.setOnAction(w->{
               items.remove(li);
               listPane.getChildren().remove(li.listText);
               listPane.getChildren().remove(li.removeButton);
               
           });
           
           y++;
       });
       
       
       okButton.setOnAction(e->{
           for (ListItem li: items){
               li.setItem(li.listText.getText());
           }
           //ListComponent lc = new ListComponent(items);
           //page.components.add(lc);
           //addListComponent(lc);
           y=0;
           page.setSelectedComponent(lc);
           listStage.close();
           
           
             
       });
       
        listStage.showAndWait();
    }
    
    public void videoPrompt(Page page){
        Stage videoStage = new Stage();
        
        Button videoSelect = new Button("Choose a Video");
        Label widthLabel = new Label("Set Video Width");
        Label heightLabel = new Label("Set Video Height");
        Label captionLabel = new Label("Set Video Caption");
        Button ok = new Button ("Okay");
        videoSelect.setMinWidth(150);
        ok.setMinWidth(150);
        TextField widthText = new TextField();
        TextField heightText = new TextField();
        TextField captionText = new TextField();
        // set allignment
        GridPane p = new GridPane();
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        GridPane pane1 = new GridPane();
        pane1.setAlignment(Pos.CENTER);
        GridPane pane2 = new GridPane();
        pane2.setAlignment(Pos.CENTER);
        GridPane pane3 = new GridPane();
        pane3.setAlignment(Pos.CENTER);
        p.add(widthLabel, 10, 0);
       // p.add(new Label(""), 10, 15);
        p.add(widthText, 10, 25);
        p.add(new Label(""), 10, 35);
        p.add(heightLabel, 10, 45);
        // p.add(new Label(""), 10, 55);
        p.add(heightText, 10, 65);
         p.add(new Label(""), 10, 75);
         p.add(captionLabel, 10, 85);
         //p.add(new Label(""), 10, 95);
        p.add(captionText, 10, 105);
         p.add(new Label(""), 10, 115);
        p.add(videoSelect, 10, 125);
        p.add(new Label(""), 10, 135);
        p.add(ok,10,145);
        
        p.getStyleClass().add("btYES");
        Scene scene = new Scene(p, 350, 250);
        scene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
        videoSelect.getStyleClass().add("btYES");
        videoStage.setTitle("Choose a Video");
        videoStage.setScene(scene);
        
        PageEditView test = this;
        ///if yes is selected
        videoSelect.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
             mediaController.processSelectVideo(page, test);
            page.setSelected(true);
            maker.getPortfolio().selected(page);
            page.setSelected(true);
            maker.getPortfolio();
            //captionVBox.setOpacity(.25);
            //imageSelectionView.setOpacity(.25);
        }
        });
    
         ok.setOnAction(e -> {
            //Component comp = new Component();
             
           int width;
            int height;
            try{
                width = Integer.parseInt(widthText.getText());
            }catch (Exception w){ width = 500;}
            try{
                  height = Integer.parseInt(heightText.getText());
            }catch (Exception q){  height = 500;}
            String caption = captionText.getText();
            
            
            VideoComponent video = new VideoComponent(width, height, caption, currentVideoPath);
            video.name = currentVideoName;
            page.components.add(video);
            page.setSelectedComponent(video);
            //page.setHeader(headerText.getText());
            addVideoComponent(video);
             videoStage.close();
        });
        
        
        videoStage.showAndWait();
          
    
    }
    
    
    public void editVideo(VideoComponent vc){
        Stage videoStage = new Stage();
        
        Button videoSelect = new Button("Choose a Video");
        Label widthLabel = new Label("Set Video Width");
        Label heightLabel = new Label("Set Video Height");
        Label captionLabel = new Label("Set Video Caption");
        Button ok = new Button ("Okay");
        videoSelect.setMinWidth(150);
        ok.setMinWidth(150);
        TextField widthText = new TextField(Integer.toString(vc.width));
        TextField heightText = new TextField(Integer.toString(vc.height));
        TextField captionText = new TextField(vc.caption);
        // set allignment
        GridPane p = new GridPane();
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        GridPane pane1 = new GridPane();
        pane1.setAlignment(Pos.CENTER);
        GridPane pane2 = new GridPane();
        pane2.setAlignment(Pos.CENTER);
        GridPane pane3 = new GridPane();
        pane3.setAlignment(Pos.CENTER);
        p.add(widthLabel, 10, 0);
       // p.add(new Label(""), 10, 15);
        p.add(widthText, 10, 25);
        p.add(new Label(""), 10, 35);
        p.add(heightLabel, 10, 45);
        // p.add(new Label(""), 10, 55);
        p.add(heightText, 10, 65);
         p.add(new Label(""), 10, 75);
         p.add(captionLabel, 10, 85);
         //p.add(new Label(""), 10, 95);
        p.add(captionText, 10, 105);
         p.add(new Label(""), 10, 115);
        p.add(videoSelect, 10, 125);
        p.add(new Label(""), 10, 135);
        p.add(ok,10,145);
        
        p.getStyleClass().add("btYES");
        Scene scene = new Scene(p, 350, 250);
        scene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
        videoSelect.getStyleClass().add("btYES");
        videoStage.setTitle("Choose a Video");
        videoStage.setScene(scene);
        
        PageEditView test = this;
        ///if yes is selected
        videoSelect.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
             mediaController.processSelectVideo(page, test);
            page.setSelected(true);
            maker.getPortfolio().selected(page);
            page.setSelected(true);
            maker.getPortfolio();
            //captionVBox.setOpacity(.25);
            //imageSelectionView.setOpacity(.25);
        }
        });
    
         ok.setOnAction(e -> {
            //Component comp = new Component();
           int width;
            int height;
            try{
                width = Integer.parseInt(widthText.getText());
            }catch (Exception w){ width = 500;}
            try{
                  height = Integer.parseInt(heightText.getText());
            }catch (Exception q){  height = 500;}
            String caption = captionText.getText();
            vc.width = width;
            vc.height= height;
            vc.caption = caption;
            vc.path = currentVideoPath;
            vc.name = currentVideoName;
            page.setSelectedComponent(vc);
            
           // VideoComponent video = new VideoComponent(width, height, caption, currentVideoPath);
            //page.components.add(video);
            //page.setHeader(headerText.getText());
            //addVideoComponent(video);
             videoStage.close();
        });
        
        
        videoStage.showAndWait();
    }  
      
      
      public void imagePrompt(Page page){
        Stage imageStage = new Stage();
        
        ObservableList<String> floats = FXCollections.observableArrayList("Right", "Left", "Neither");
                final ComboBox floatBox = new ComboBox(floats);
                //layoutBox.setPromptText("Layout");
                floatBox.setStyle("-fx-text-align: center");
        
                floatBox.setMinWidth(150);
                
        Button imageSelect = new Button("Choose an Image");
        Button ok = new Button ("Okay");
        imageSelect.setMinWidth(150);
        imageSelect.setMaxWidth(150);
        ok.setMinWidth(150);
        Label widthLabel = new Label("Set Image Width");
        Label heightLabel = new Label("Set Image Height");
        Label captionLabel = new Label("Set Image Caption");
        Label floatLabel = new Label("Set Image Float");
        TextField widthText = new TextField();
        TextField heightText = new TextField();
        TextField captionText = new TextField();
        // set allignment
        GridPane p = new GridPane();
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        GridPane pane1 = new GridPane();
        pane1.setAlignment(Pos.CENTER);
        GridPane pane2 = new GridPane();
        pane2.setAlignment(Pos.CENTER);
        GridPane pane3 = new GridPane();
        pane3.setAlignment(Pos.CENTER);
        p.add(widthLabel, 10, 0);
       // p.add(new Label(""), 10, 15);
        p.add(widthText, 10, 25);
        p.add(new Label(""), 10, 35);
        p.add(heightLabel, 10, 45);
        // p.add(new Label(""), 10, 55);
        p.add(heightText, 10, 65);
         p.add(new Label(""), 10, 75);
         p.add(captionLabel, 10, 85);
         //p.add(new Label(""), 10, 95);
        p.add(captionText, 10, 105);
         p.add(new Label(""), 10, 115);
         p.add(floatLabel, 10, 125);
         //p.add(new Label(""), 10, 95);
        p.add(floatBox, 10, 135);
         p.add(new Label(""), 10, 145);
        p.add(imageSelect, 10, 155);
        p.add(new Label(""), 10, 165);
        p.add(ok, 10, 175);
        
        
        
        p.getStyleClass().add("btYES");
        Scene scene = new Scene(p, 350, 300);
        scene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
        imageSelect.getStyleClass().add("btYES");
        imageStage.setTitle("Choose an Image");
        imageStage.setScene(scene);
        
        PageEditView test = this;
        ///if yes is selected
        imageSelect.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
             mediaController.processSelectImage(page, test);
            page.setSelected(true);
            maker.getPortfolio().selected(page);
            page.setSelected(true);
            maker.getPortfolio();
            //captionVBox.setOpacity(.25);
            //imageSelectionView.setOpacity(.25);
            imageSelect.setText(currentImagePath);
        }
        });
        
        
        ok.setOnAction(e -> {
            //Component comp = new Component();
            int width;
            int height;
            try{
                width = Integer.parseInt(widthText.getText());
            }catch (Exception w){ width = 500;}
            try{
                  height = Integer.parseInt(heightText.getText());
            }catch (Exception q){  height = 500;}
            String caption = captionText.getText();
            String floated;
            if (floatBox.getValue() != null){
                 floated = floatBox.getValue().toString();
             }
            else{floated = "Neither";}
            
            ImageComponent image = new ImageComponent(width, height, caption, floated, currentImagePath);
            image.name=currentImageName;
            page.components.add(image);
            page.setSelectedComponent(image);
            //page.setHeader(headerText.getText());
            addImageComponent(image);
             imageStage.close();
        });
        
    
        imageStage.showAndWait();
          
    
        }
      
    public void editImage(ImageComponent ic){
        Stage imageStage = new Stage();
        
        ObservableList<String> floats = FXCollections.observableArrayList("Right", "Left", "Neither");
                final ComboBox floatBox = new ComboBox(floats);
                //layoutBox.setPromptText("Layout");
                floatBox.setStyle("-fx-text-align: center");
        
                floatBox.setMinWidth(150);
                floatBox.setValue(ic.floated);
                
        Button imageSelect = new Button(ic.path);
        Button ok = new Button ("Okay");
        imageSelect.setMinWidth(150);
        imageSelect.setMaxWidth(150);
        ok.setMinWidth(150);
        Label widthLabel = new Label("Set Image Width");
        Label heightLabel = new Label("Set Image Height");
        Label captionLabel = new Label("Set Image Caption");
        Label floatLabel = new Label("Set Image Float");
        TextField widthText = new TextField(Integer.toString(ic.width));
        TextField heightText = new TextField(Integer.toString(ic.height));
        TextField captionText = new TextField(ic.caption);
        // set allignment
        GridPane p = new GridPane();
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        GridPane pane1 = new GridPane();
        pane1.setAlignment(Pos.CENTER);
        GridPane pane2 = new GridPane();
        pane2.setAlignment(Pos.CENTER);
        GridPane pane3 = new GridPane();
        pane3.setAlignment(Pos.CENTER);
        p.add(widthLabel, 10, 0);
       // p.add(new Label(""), 10, 15);
        p.add(widthText, 10, 25);
        p.add(new Label(""), 10, 35);
        p.add(heightLabel, 10, 45);
        // p.add(new Label(""), 10, 55);
        p.add(heightText, 10, 65);
         p.add(new Label(""), 10, 75);
         p.add(captionLabel, 10, 85);
         //p.add(new Label(""), 10, 95);
        p.add(captionText, 10, 105);
         p.add(new Label(""), 10, 115);
         p.add(floatLabel, 10, 125);
         //p.add(new Label(""), 10, 95);
        p.add(floatBox, 10, 135);
         p.add(new Label(""), 10, 145);
        p.add(imageSelect, 10, 155);
        p.add(new Label(""), 10, 165);
        p.add(ok, 10, 175);
        
        
        
        p.getStyleClass().add("btYES");
        Scene scene = new Scene(p, 350, 300);
        scene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
        imageSelect.getStyleClass().add("btYES");
        imageStage.setTitle("Choose an Image");
        imageStage.setScene(scene);
        
        PageEditView test = this;
        ///if yes is selected
        imageSelect.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
             mediaController.processSelectImage(page, test);
            page.setSelected(true);
            maker.getPortfolio().selected(page);
            page.setSelected(true);
            maker.getPortfolio();
            imageSelect.setText(currentImagePath);
            //captionVBox.setOpacity(.25);
            //imageSelectionView.setOpacity(.25);
        }
        });
        
        
        ok.setOnAction(e -> {
            //Component comp = new Component();
            int width;
            int height;
            try{
                width = Integer.parseInt(widthText.getText());
            }catch (Exception w){ width = 500;}
            try{
                  height = Integer.parseInt(heightText.getText());
            }catch (Exception q){  height = 500;}
            String caption = captionText.getText();
            String floated;
            if (floatBox.getValue() != null){
                 floated = floatBox.getValue().toString();
             }
            else{floated = "Neither";}
            
            ic.width = width;
            ic.height = height;
            ic.caption = caption;
            ic.floated = floated;
            ic.path = currentImagePath;
            ic.name = currentImageName;
            page.setSelectedComponent(ic);
            
            //ImageComponent image = new ImageComponent(width, height, caption, floated, currentImagePath);
            //page.components.add(image);
            //page.setHeader(headerText.getText());
            //addImageComponent(image);
             imageStage.close();
        });
        
    
        imageStage.showAndWait();
          
    
        
    }
      
      
      public void slideShowPrompt(Page page){
          Stage ssStage = new Stage();
          
          SlideShowMakerView ssmv = new SlideShowMakerView();
          ssmv.startUI(ssStage, "Slide Show Maker", this);
          SlideShowComponent ssc = new SlideShowComponent(slides, slideShowMakerView);
          addSlideShowComponent(ssc);
          page.components.add(ssc);
          page.setSelectedComponent(ssc);
      }
      
      
      public void editSlideShow(SlideShowComponent sc){
          Stage ssStage = new Stage();
          sc.ssmv.startUI(ssStage, "Slide Show Maker", this);
          sc.ssmv.reloadSlideShowPane(sc.ssmv.getSlideShow());
          sc.ssmv.updateToolbarControls(false);
          page.setSelectedComponent(sc);
      }
      
          public void updateSlideImage() {
	String imagePath = "./images/slide_show_images/DefaultStartSlide.png";
        //if (slide.getCaption() != null){  setCaption(slide.getCaption()); }
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = 100;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
	    // @todo - use Error handler to respond to missing image
	}
    }   
      
      
      public void setTab(Tab t){
          tab=t;
      }
     public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String tooltip, 
	    String cssClass,
	    boolean disabled) {
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + "./images/icons/"+ iconFileName;
	Image buttonImage = new Image(imagePath);
        System.out.println(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
      
      
}
