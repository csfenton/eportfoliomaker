/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.view;
import eportfoliomaker.component.Component;
import eportfoliomaker.component.HeaderComponent;
import eportfoliomaker.component.ImageComponent;
import eportfoliomaker.component.ListComponent;
import eportfoliomaker.component.ParagraphComponent;
import eportfoliomaker.component.SlideShowComponent;
import eportfoliomaker.component.VideoComponent;
import eportfoliomaker.file.EPortfolioFileManager;
import eportfoliomaker.controller.FileController;
import eportfoliomaker.controller.PortfolioEditController;
import eportfoliomaker.model.EPortfolioModel;
import eportfoliomaker.model.Page;
import javafx.geometry.Orientation;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author Chris
 */
public class EPortfolioMakerView {
     // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    
    int switcher = 0;
    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    FlowPane workspaceToolbarPane;
    Button newPortfolioButton;
    Button loadPortfolioButton;
    Button savePortfolioButton;
    Button viewPortfolioButton;
    Button exitButton;
    Button switchViewButton;
    Button exportPortfolioButton;
    Button saveAsPortfolioButton;
    
    // WORKSPACE
    public HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    FlowPane pageEditToolbar;
    Button addPageButton;
    Button moveUpButton;
    Button moveDownButton;
    Button removePageButton;
    
    // AND THIS WILL GO IN THE CENTER
    ScrollPane pageEditorScrollPane;
    TabPane pageEditorTabPane;
    
    VBox pageEditorPane;

    // THIS IS THE PORTFOLIO WE'RE WORKING WITH
    EPortfolioModel portfolio;
    int y = 0;
   
    
    
    
  

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    EPortfolioFileManager fileManager;
    public String saveName;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    //private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private PortfolioEditController editController;
    
    public Page selectedPage;
    
    
    
    public EPortfolioMakerView(EPortfolioFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// MAKE THE DATA MANAGING MODEL
	portfolio = new EPortfolioModel(this);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	//errorHandler = new ErrorHandler(this);
        
        
        
        
    }
    
    // ACCESSOR METHODS
    public EPortfolioModel getPortfolio(){
        return portfolio;
    }
    
    public Stage getWindow() {
	return primaryStage;
    }
    
    public BorderPane getBorderPane(){
        return ssmPane;
    }
    
    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        
        
        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle);
    }
    
    
    
    
  
    
    
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
     private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
         //fileToolbarPane.setPadding(new Insets(5, 0, 5, 0));
    
        fileToolbarPane.setHgap(4);
        fileToolbarPane.getStyleClass().add("tool_bar");
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	newPortfolioButton = initChildButton(fileToolbarPane, "New.png",	"New ePortfolio",	    "horizontal_toolbar_button", false);
	loadPortfolioButton = initChildButton(fileToolbarPane, "Load.png",	"Load ePortfolio",    "horizontal_toolbar_button", false);
	savePortfolioButton = initChildButton(fileToolbarPane, "Save.png",	"Save ePortfolio",    "horizontal_toolbar_button", true);
        saveAsPortfolioButton = initChildButton(fileToolbarPane, "SaveAs.png",	"Save ePortfolio As",    "horizontal_toolbar_button", true);
	//viewPortfolioButton = initChildButton(fileToolbarPane, "View.png",	"View Site",    "horizontal_toolbar_button", true);
        exportPortfolioButton = initChildButton(fileToolbarPane, "Export.png",	"Export ePortfolio",    "horizontal_toolbar_button", true);
	exitButton = initChildButton(fileToolbarPane, "Exit.png", "Exit", "horizontal_toolbar_button", false);
       
        
        workspaceToolbarPane = new FlowPane();
        workspaceToolbarPane.getStyleClass().add("view_switch");
        switchViewButton = initChildButton(workspaceToolbarPane, "View.png",	"Switch View Mode",    "horizontal_toolbar_button", true);
    }
    
     
     /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	ssmPane.setCenter(workspace);
        //Image image = new Image("ssmicon.png");
	//ssmPane.setRight(new ImageView(image));
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	savePortfolioButton.setDisable(saved);
        saveAsPortfolioButton.setDisable(saved);
        exportPortfolioButton.setDisable(saved);
        switchViewButton.setDisable(saved);
	
	fileController.markFileAsNotSaved();
	// AND THE SLIDESHOW EDIT TOOLBAR
	addPageButton.setDisable(false);
    }
     
     
     
     
     
     /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String tooltip, 
	    String cssClass,
	    boolean disabled) {
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + "./images/icons/"+ iconFileName;
	Image buttonImage = new Image(imagePath);
        System.out.println(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    
    
    
    
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
	workspace.getStyleClass().add("work-space");
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
        Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();
	pageEditToolbar = new FlowPane(Orientation.VERTICAL);
        pageEditToolbar.setVgap((bounds.getHeight() /4)  +3);
	pageEditToolbar.getStyleClass().add("portfolio_edit_vbox");
	addPageButton = this.initChildButton(pageEditToolbar,		"Add.png",	    "Add Page",	    "vertical_toolbar_button",  true);
       // moveUpButton = this.initChildButton(pageEditToolbar,		ICON_MOVE_UP,	    TOOLTIP_MOVE_UP,	    "vertical_toolbar_button",  true);
        //moveDownButton = this.initChildButton(pageEditToolbar,		ICON_MOVE_DOWN,	    TOOLTIP_MOVE_DOWN,	    "vertical_toolbar_button",  false);
        removePageButton = this.initChildButton(pageEditToolbar,	"Remove.png",	    "Remove Page",	    "vertical_toolbar_button",  true);
	
	// AND THIS WILL GO IN THE CENTER
	pageEditorPane = new VBox();
	pageEditorTabPane = new TabPane();//pageEditorPane);
        pageEditorTabPane.getStyleClass().add("page_edit_view");
        
       // slidesEditorScrollPane.getStyleClass().add("work_space");
	//slidesEditorPane.getStyleClass().add("work_space");
	// NOW PUT THESE TWO IN THE WORKSPACE
        
        Button testButton = new Button("            test         ");
        testButton.minWidth(500);
        //pageEditorPane.getChildren().add(testButton);
        pageEditorPane.minWidth(1000);
        pageEditorTabPane.minWidth(1000);
        
	workspace.getChildren().add(pageEditToolbar);
	workspace.getChildren().add(pageEditorTabPane);
    }
    
    
    
    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	fileController = new FileController(this, fileManager);
        newPortfolioButton.setOnAction(e -> {
	    fileController.handleNewPortfolioRequest();
	});
        exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
	});
        loadPortfolioButton.setOnAction(e -> {
	    fileController.handleLoadPortfolioRequest();
	});
        saveAsPortfolioButton.setOnAction(e -> {
	    fileController.handleSaveAsPortfolioRequest(this);
	});
        savePortfolioButton.setOnAction(e -> {
            
	    fileController.handleSavePortfolioRequest(this);
	});
        exportPortfolioButton.setOnAction(e -> {
            
	    fileController.handleExportPortfolioRequest(this);
	});
        switchViewButton.setOnAction(e -> {
	    fileController.handleSwitchViewRequest(this, y);
            if (y==0){
                y++;
            }
            else{
                y--;
            }
	});
        editController = new PortfolioEditController(this);
        addPageButton.setOnAction(e -> {
	    editController.processAddPageRequest();
            fileController.markFileAsNotSaved();
	});
        removePageButton.setOnAction(e->{
            editController.processRemovePageRequest(selectedPage);
            fileController.markFileAsNotSaved();
        });
    }
    
    
    
    
    
     private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();
        
	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        
        //Image image = new Image("ssmicon.png");
        HBox pic = new HBox();
        //pic.getChildren().add(image);
        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ssmPane = new BorderPane();
        VBox top = new VBox();
        top.getChildren().add(fileToolbarPane);
        top.getChildren().add(workspaceToolbarPane);
	//ssmPane.setTop(fileToolbarPane);
        //ssmPane.setTop(workspaceToolbarPane);
        ssmPane.setTop(top);
        //ssmPane.setCenter(new ImageView(image));
            //add space to enter title
                //PropertiesManager props = PropertiesManager.getPropertiesManager();
                //Label l =new Label((props.getProperty(COURSE_TITLE_LABEL.toString())));
                //l.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON);
                //fileToolbarPane.getChildren().addAll(l, title);
                
                //stringTitle = title.getText();
                
                
	primaryScene = new Scene(ssmPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add("eportfoliomaker/style/EPortfolioStyle.css");
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    
   /**
     * Uses the portfolio data to reload all the components for
     * page editing.
     * 
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSlideShowPane(EPortfolioModel portfolioToLoad) {
        
            pageEditorPane.getChildren().clear();
            pageEditorTabPane.getTabs().clear();
        
	
	for (Page page : portfolioToLoad.getPages()) {
	    PageEditView slideEditor = new PageEditView(page, this);
            //slide.setCaption(slideEditor.getCaption());
            //slideEditor.setCaption(slide.getCaption());
            
            Tab tab = new Tab();
            tab.setText("New Page");
            //Preserve banner image
            if (page.getBannerImage() != null){
                slideEditor.bannerButton.setText(page.getBannerImage());
            }
            //Preserve page name
            if (page.getTitle() != null){
                slideEditor.pageNameText.setText(page.getTitle());
                tab.setText(page.getTitle());
            }
            
            //Preserve Student Name
             if (page.getName() != null){
                slideEditor.studentNameText.setText(page.getName());
            }
             
            //Preserve footer
             if (page.getFooterText() != null){
                slideEditor.footerText.setText(page.getFooterText());
            }
             
             //Preserve Font
              if (page.getFont() != null){
                slideEditor.fontBox.setValue(page.getFont());
            }
              
              //Preserve color
              if (page.getColor() != null){
                slideEditor.colorBox.setValue(page.getColor());
            }
              
              
              //Preserve Layout
              if (page.getLayout() != null){
                slideEditor.layoutBox.setValue(page.getLayout());
            }
              
            //Preserve Components
              if (page.components !=null){
              if (page.components.isEmpty() == false){
                  for (Component c: page.components){
                     if (c instanceof HeaderComponent){
                      slideEditor.addHeaderComponent((HeaderComponent) c);
                    }
                     if (c instanceof ParagraphComponent){
                      slideEditor.addParagraphComponent((ParagraphComponent) c);
                    }
                     if (c instanceof ListComponent){
                      slideEditor.addListComponent((ListComponent) c);
                    }
                     if (c instanceof VideoComponent){
                      slideEditor.addVideoComponent((VideoComponent) c);
                    }
                     if (c instanceof ImageComponent){
                      slideEditor.addImageComponent((ImageComponent) c);
                    }
                     if (c instanceof SlideShowComponent){
                      slideEditor.addSlideShowComponent((SlideShowComponent) c);
                    }
                  }
              }
              }
              Screen screen = Screen.getPrimary();
              Rectangle2D bounds = screen.getVisualBounds();
             ScrollPane sp = new ScrollPane(slideEditor);
             sp.setMinHeight(bounds.getHeight());
            tab.setContent(sp);
            
            tab.setOnSelectionChanged(e->{
                selectedPage = page;
            });
            
            
            pageEditorTabPane.getTabs().add(tab);
            slideEditor.setTab(tab);
            
	   // pageEditorPane.getChildren().add(slideEditor);
            if (portfolio.isPageSelected()){
                removePageButton.setDisable(false);
            }
            else { removePageButton.setDisable(true);}
            
            removePageButton.setDisable(false);
	}
        if (portfolioToLoad.getPages().isEmpty()){
            removePageButton.setDisable(true);
        }
       // stringTitle = title.getText();
        //if (stringTitle.length() > 0){
       //     slideShowToLoad.setTitle(stringTitle);
            
       // }
    }
    
    
    
}
