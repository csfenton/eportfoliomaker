/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.model;

import eportfoliomaker.component.Component;
import eportfoliomaker.view.PageEditView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Chris
 */
public class Page {
    String title;
    String name;
    String font;
    String layout;
    String color;
    String header;
    String bannerImage;
    String bannerName;
    String footerText;
    public ObservableList<Component> components;
    PageEditView editor;
    Component selectedComponent;
    boolean selected;
    boolean isSelected;
    
    public Page(){
        selected = true;
        isSelected = true;
        components = FXCollections.observableArrayList();
        title = "new page";
        footerText = "";
       // bannerImage = "Choose /n banner immage";
        layout = "Layout 1";
        font = "Georgia";
        color = "219 Theme";
    }
    
    public Page(String title, String name, String font, String layout, String color, String banner, String footerText, ObservableList<Component> components){
        this.title = title;
        this.name= name;
        this.font =font;
        this.layout = layout;
        this.color = color;
        this.bannerImage = banner;
        this.footerText = "Prodeuced by ePortfolioMaker";;
        if (this.footerText == null) this.footerText = "Prodeuced by ePortfolioMaker"; 
        this.components=components;
        selected = true;
        isSelected = true;
        components = FXCollections.observableArrayList();
    }
    
    
    //ACCESSOR METHODS
    
    public String getTitle(){
        return title;
    }
    
    public String getName(){
        return name;
    }
    
    public String getFont(){
        return font;
    }
    
    public String getLayout(){
        return layout;
    }
    
    public String getColor(){
        return color;
    }
    
    public String getHeader(){
        return header;
    }
    
    public String getBannerImage(){
        return bannerImage;
    }
    
    public String getFooterText(){
        return footerText;
    }
    
    public boolean getSelected(){
        return selected;
    }
    
    public Component getSelectedComponent(){
        return selectedComponent;
        
    }
    
    public String getBannerName(){
        return bannerName;
    }
    
    
    //MUTATOR METHODS
    public void setTitle(String a){
        title = a;
        System.out.println(title);
    }
    
     public void setName(String a){
        name = a;
        System.out.println(name);
    }
     
    public void setFont(String a){
        font = a;
        System.out.println(a);
    }
      
    public void setColor(String a){
        color = a;
        System.out.println(a);
    }
       
    public void setLayout(String a){
        layout = a;
        System.out.println(a);
    }
    
    public void setHeader(String a){
        header = a;
        //System.out.print(a);
        for (Component c : components){
            System.out.println(c.name);
        }
    }
        
     public void setBannerImage(String a){
        bannerImage = a;
        System.out.println(a);
    }
     
    public void setBannerName(String name){
        bannerName = name;
    }
          
     public void setFooterText(String a){
        footerText = a;
        
    }
    
    public void setEditor(PageEditView ed){
        editor =ed;
    }
    
     public void setSelected(boolean s){
         selected = s;
         if (s == true){
             //editor.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, null, null)));
             //editor.setOpacity(.50);
             
         }
         
         if (selected == false){
            // editor.setOpacity(10);
         }
     }
    
     
     public void setSelectedComponent(Component c){
         selectedComponent = c;
         for (Component component: components){
             component.compButton.setOpacity(.61);
         }
         selectedComponent.compButton.setOpacity(1);
     }
    
    
    
}



 