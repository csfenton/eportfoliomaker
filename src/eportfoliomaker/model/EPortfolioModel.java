/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker.model;

import eportfoliomaker.component.Component;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import eportfoliomaker.view.EPortfolioMakerView;

/**
 *
 * @author Chris
 */
public class EPortfolioModel {
    public EPortfolioMakerView ui;
    public String title ="title";
    public ObservableList<Page> pages;
    public Page selectedPage;
    public String saveName ="sn";
    
    public EPortfolioModel(EPortfolioMakerView initUI) {
	ui = initUI;
        //this.setTitle(ui.getTitle());
	pages = FXCollections.observableArrayList();
	reset();	
    }
    
    
    /**
     * Adds a page to the site with the parameter settings.
     
     */
    

    public void addPage() {
	Page pageToAdd = new Page();
	
        pages.add(pageToAdd);
	selectedPage = pageToAdd;
	ui.reloadSlideShowPane(this);
        
    }
    
    public void addPage(String title, String name, String font, String layout, String color, String banner, String footerText, ObservableList<Component> components){
        Page pageToAdd = new Page(title,  name, font,  layout, color, banner, footerText, components);
        pages.add(pageToAdd);
        selectedPage = pageToAdd;
        ui.reloadSlideShowPane(this);
    }
    
    public void addPage(Page page){
        pages.add(page);
        selectedPage = page;
        ui.reloadSlideShowPane(this);
        System.out.println("fuck");
    }
    
    public void removePage(Page page){
        pages.remove(page);
        ui.reloadSlideShowPane(this);
    }
    
    public ObservableList<Page> getPages() {
        //setCaption();
	return pages;
    }
    
    
     public void reset() {
        
	pages.clear();
	//ui.reloadSlideShowPane(this);
	title = "ePortfolioMaker";
	selectedPage = null;
    }
     
     public void reset(int i){
         pages.clear();
	ui.reloadSlideShowPane(this);
	title = "ePortfolioMaker";
	selectedPage = null;
     }
    
    public void selected(Page selecslide){
        selectedPage = selecslide;
        for (Page page : pages){
            page.setSelected(false);
        }
        //selectedSlide.setSelected(true);
        
        
    }
    
    
     // ACCESSOR METHODS
    public boolean isPageSelected() {
	//return selectedSlide != null;
        boolean selec = false;
        for (Page page : pages) {
	  if (page.getSelected()){
              return true;
          } 
        }
        return false;
        
    }
    
    public String getTitle(){
        return title;
    }
    public String getName(){
        return saveName;
    }
    
    public void setName(String name){
        saveName= name;
    }
    
    
}
